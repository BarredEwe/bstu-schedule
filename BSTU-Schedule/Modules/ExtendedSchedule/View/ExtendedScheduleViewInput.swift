//
//  ExtendedScheduleExtendedScheduleViewInput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 03/05/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

protocol ExtendedScheduleViewInput: class {

    /**
        @author Maxim
        Setup initial state of the view
    */

    func setupInitialState()
}
