import UIKit

class APIRoom: APIMGObject {
    
    class func getRoomsWith(name: String, success: @escaping ([MGObject]?) -> ()) {
        
        let objectsFromDB = RoomDB().getRooms(filter: name)
        success(objectsFromDB)
        
        self.getMGObjectWith(type: "lectureHall", name: name, success: { responseObject in
            let objects = MGObjectMapper.objectsFromResponce(responseObject: responseObject, type: "data")
            if objects != nil {
                var rooms = [Room]()
                for object in objects! {
                    rooms.append(RoomMapper.objectFrom(object: object))
                }
                DispatchQueue.global(qos: .userInitiated).async {
                    RoomDB().fetchRooms(groups: rooms)
                }
            }
            success(objects)
        }, failure: { error in
            
        })
    }
    
}
