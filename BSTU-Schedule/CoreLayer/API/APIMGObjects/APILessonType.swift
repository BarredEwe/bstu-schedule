import UIKit

class APILessonType: APIMGObject {
    
    class func getLessonTypeWith(name: String, success: @escaping ([MGObject]?) -> ()) {

        self.getMGObjectWith(type: "eventType", name: name, success: { responseObject in
            
            guard let objects = MGObjectMapper.objectsFromResponce(responseObject: responseObject, type: "data") else {
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).async {
                LessonTypeDB().fetchLessonTypes(objects: objects)
            }
            success(objects)
            
        }, failure: { error in
            
        })
    }
    
}
