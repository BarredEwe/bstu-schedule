protocol SignInViewOutput {
    
    func viewIsReady()
    func closeView()
    func showErrorAuthorizationAlert()
    func userAuthorization(userName: String, password: String)
    
}
