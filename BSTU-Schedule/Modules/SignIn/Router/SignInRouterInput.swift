import Foundation

protocol SignInRouterInput {
    
    func closeView()
    func showErrorAuthorizationAlert()

}
