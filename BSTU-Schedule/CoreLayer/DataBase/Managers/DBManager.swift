import RealmSwift

class DBManager {
    
    let realm = try! Realm()
    
    func getObject<T: Object>(type: T.Type) -> T? {
        return realm.objects(type).first
    }
    
    func removeAllObjectOfType<T: Object>(type: T.Type) {
        let object = realm.objects(type)
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print("DataBase error with remove object: ", object.description)
        }
    }
    
    func removeObjects(key: String) {
        let realm = try! Realm()
        if let obj = realm.object(ofType: Event.self, forPrimaryKey: key) {
            do {
                try realm.write {
                    realm.delete(obj)
                }
            } catch {
                print("DataBase error with remove objects: ", obj.description)
            }
        }
    }
    
    func removeObject(object: Object) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print("DataBase error with delete object: ", object.description)
        }
    }
    
    func getObjectsOf<T: Object>(type: T.Type, predicate: NSPredicate) -> Results<T>? {
        let obj = realm.objects(type).filter(predicate)
        return obj
    }
    
    func addObjectAsync<T: Object>(object: [T], udpate: Bool = false) {
        
        let realm = try! Realm()
        
        //DispatchQueue.main.async {
        for obj in object {
            realm.beginWrite()
            realm.add(obj, update: udpate)
            try! realm.commitWrite()
        }
        //}
        
    }
    
    func removeObjects(predicate: NSPredicate) {
        let obj = realm.objects(Event.self).filter(predicate)
        
        do {
            try realm.write {
                realm.delete(obj)
            }
        } catch {
            print("DataBase error with remove objects: ", obj.first!.description)
        }
    }
    
    func addObject<T: Object>(object: T, udpate: Bool = false) {
        do {
            try realm.write {
                realm.add(object, update: udpate)
            }
        } catch {
            print("DataBase error with append object: ", object.description)
        }
    }
    
    func append<T: Object>(object: T, to cur: List<T>) {
        do {
            try realm.write {
                cur.append(object)
            }
        } catch {
            print("DataBase error with append object: ", object.description)
        }
    }
    
    func convertTo<T: Object, C: Object>(objects: Results<T>, toType: C) -> [C] {
        var toObjects = [C]()
        for object in objects {
            toObjects.append(object as! C)
        }
        return toObjects
    }
    
}
