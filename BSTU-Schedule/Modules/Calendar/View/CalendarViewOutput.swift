import JTAppleCalendar

protocol CalendarViewOutput {
    
    func viewIsReady()
    func viewDidSelect(date: Date)
    func startDate() -> Date
    func endDate() -> Date
    func selectedDates() -> [Date]
    func navigateBack()
    
}
