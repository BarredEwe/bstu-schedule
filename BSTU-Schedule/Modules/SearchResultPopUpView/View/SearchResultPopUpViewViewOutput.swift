
protocol SearchResultPopUpViewViewOutput {
    
    func viewIsReady()
    
    func startSearchWith(data: String, type: TypeSearch)
    func getObjects() -> [MGObject]
    func didFinishSearch(index: Int)
    func getLastSearch() -> String
}
