import UIKit

class ExtendedScheduleAssembly {
    
    class func assemblyWith(event: Event, filter: Filter) -> UIViewController {
        let storyBoard = UIStoryboard(name: "ExtendedScheduleView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! ExtendedScheduleViewController
        
        let router = ExtendedScheduleRouter()
        router.view = vc
        
        let presenter = ExtendedSchedulePresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = ExtendedScheduleInteractor()
        interactor.setEvent(event: event)
        interactor.setFilter(filter: filter)
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
