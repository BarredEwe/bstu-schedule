import RealmSwift
import UIKit

class MGObjectDB: DBManager {
    
    func fetchObjects<T: Object>(groups: [T]) {
        for group in groups {
            self.addObject(object: group, udpate: true)
        }
    }
    
    func getObjects<T: Object>(filter: String, type: T) -> [T]? {
        
        let url = "name CONTAINS[cd] %@"
        
        let predicate = NSPredicate(format: url, argumentArray: [filter])
        
        guard let objects = self.getObjectsOf(type: T.self, predicate: predicate) else {
            return nil
        }
        return GroupMapper.convertTo(objects: objects, toType: T())
        
    }
    
}
