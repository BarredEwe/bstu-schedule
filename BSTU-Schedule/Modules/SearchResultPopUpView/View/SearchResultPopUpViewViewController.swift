import UIKit

class SearchResultPopUpView: UIView, SearchResultPopUpViewViewInput, UITableViewDelegate, UITableViewDataSource, SendSearchDelegate {

    var output: SearchResultPopUpViewViewOutput!

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        output.viewIsReady()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SearchResultCell", bundle: nil), forCellReuseIdentifier: "SearchResultCell")
    }

    // MARK: SearchResultPopUpViewViewInput
    func setupInitialState() {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.output.getObjects().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell") as! SearchResultCell
        
        let string = StringToAttributeString.stringChangeAttr(string: self.output.getObjects()[indexPath.row].name, searchString: self.output.getLastSearch())
        cell.infoLabel?.attributedText = string
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.output.didFinishSearch(index: indexPath.row)
    }
    
    func updateResults() {
        self.tableView.reloadData()
    }
    
    func startSearchWith(data: String, type: TypeSearch) {
        self.output.startSearchWith(data: data, type: type)
    }
    
}
