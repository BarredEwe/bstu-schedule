import UIKit

class CalendarAssembly {
    
    class func assemblyWith(filter: Filter, delegate: SearchResultDelegate?) -> UIViewController {
        let storyBoard = UIStoryboard(name: "CalendarView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! CalendarViewController
        
        let router = CalendarRouter()
        router.view = vc
        
        let presenter = CalendarPresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = CalendarInteractor()
        interactor.setFilter(filter: filter)
        interactor.setDelegate(delegate: delegate)
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
