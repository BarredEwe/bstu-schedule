import UIKit
import RealmSwift

class UserDB: DBManager {
    
    func getUser() -> User? {
        return self.getObject(type: User.self)
    }
    
    func fetch(user: User) {
        self.addObject(object: user)
    }
    
}
