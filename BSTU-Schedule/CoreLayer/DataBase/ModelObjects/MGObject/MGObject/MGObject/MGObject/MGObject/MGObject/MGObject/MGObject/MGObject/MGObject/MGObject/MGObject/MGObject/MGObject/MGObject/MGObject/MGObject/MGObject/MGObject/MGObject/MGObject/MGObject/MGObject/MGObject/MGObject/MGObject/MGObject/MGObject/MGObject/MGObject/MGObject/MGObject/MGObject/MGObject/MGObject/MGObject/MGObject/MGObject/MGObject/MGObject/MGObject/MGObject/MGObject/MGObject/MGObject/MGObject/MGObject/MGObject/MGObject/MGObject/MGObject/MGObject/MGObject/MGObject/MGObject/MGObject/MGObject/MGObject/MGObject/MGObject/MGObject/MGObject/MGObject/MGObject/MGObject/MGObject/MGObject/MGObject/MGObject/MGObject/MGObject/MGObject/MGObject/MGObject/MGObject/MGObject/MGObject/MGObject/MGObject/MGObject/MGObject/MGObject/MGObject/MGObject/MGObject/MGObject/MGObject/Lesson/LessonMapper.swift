import UIKit

protocol LessonMapperProtocol {
    static func objectFrom(object: MGObject) -> Lesson
}

class LessonMapper: MGObjectMapper, LessonMapperProtocol {
    
    class func objectFrom(object: MGObject) -> Lesson {
        let lesson = Lesson()
        lesson.name = object.name
        lesson.id = object.id
        return lesson
    }
    
    class func objectsFrom(objects: [MGObject]) -> [Lesson] {
        var lessons = [Lesson]()
        for object in objects {
            let lesson = Lesson()
            lesson.name = object.name
            lesson.id = object.id
            lessons.append(lesson)
        }
        return lessons
    }
    
}
