import UIKit
import XCTest
@testable import BSTU_Schedule

class SearchAssemblyTests: XCTestCase {
    
    var viewController: SearchViewController!

    override func setUp() {
        super.setUp()
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModule() {
        //given
        let type = TypeSearch.disciple
        let searchDelegateMock = SearchDelegateMock()
        
        //when
        viewController = SearchAssembly.assemblyWith(type: type, delegate: searchDelegateMock) as! SearchViewController

        //then
        XCTAssertNotNil(viewController.output, "SearchViewController is nil after assembly")
        XCTAssertTrue(viewController.output is SearchPresenter, "output is not SearchPresenter")

        let presenter: SearchPresenter = viewController.output as! SearchPresenter
        XCTAssertNotNil(presenter.view, "view in SearchPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in SearchPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is SearchRouter, "router is not SearchRouter")

        let interactor: SearchInteractor = presenter.interactor as! SearchInteractor
        XCTAssertNotNil(interactor.output, "output in SearchInteractor is nil after assembly")
    }
    
    class SearchDelegateMock: SearchResultDelegate {
        
        func didFinisedSearchWith(result: Any) {
        
        }
        
    }
}
