import UIKit

class SplashScreenViewController: UIViewController {
    
    let vc = MGTabBar()

    @IBOutlet weak var background: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        print("Deinit SplashScreenViewController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let sizeShape = (self.view.bounds.height / 3.5) / 2
        let circleLayer = generateCircleLayer(size: sizeShape)
        
        self.view.layer.addSublayer(circleLayer)
        
        self.layerScaleAnimationBg(layer: background.layer)
        self.layerScaleAnimation(layer: circleLayer, duration1: 0.8, duration2: 0.8)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                
                self.navigationController?.viewControllers = [self.vc]
                self.navigationController?.popToRootViewController(animated: false)
                
            }, completion: nil)
        }
        
    }
    
    func generateCircleLayer(size: CGFloat) -> CAShapeLayer {
        let layer = CAShapeLayer()
        layer.lineWidth = size
        layer.frame = CGRect(x: UIScreen.main.bounds.width / 2 - (size / 2), y: UIScreen.main.bounds.height / 2 - (size / 2), width: size, height: size)
        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: size, height: size))
        layer.path = path.cgPath
        layer.strokeColor = UIColor.white.cgColor
        layer.fillColor = UIColor.clear.cgColor
        return layer
    }
    
    func layerScaleAnimation(layer: CALayer, duration1: CFTimeInterval, duration2: CFTimeInterval) {
        let timing = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let scaleFactor = self.view.bounds.height / layer.bounds.width
        
        let tA = CABasicAnimation(keyPath: "transform")
        tA.timingFunction = timing
        tA.duration = duration1
        tA.isRemovedOnCompletion = false
        tA.fillMode = kCAFillModeForwards
        let startingTransform = CATransform3DMakeScale(0.9, 0.9, 1)
        tA.toValue = NSValue(caTransform3D: startingTransform)
        
        let tA2 = CABasicAnimation(keyPath: "transform")
        tA2.timingFunction = timing
        tA2.duration = duration2
        tA2.beginTime = duration1
        tA2.isRemovedOnCompletion = false
        tA2.fillMode = kCAFillModeForwards
        let startingTransform2 = CATransform3DMakeScale(scaleFactor, scaleFactor, 1)
        tA2.toValue = NSValue(caTransform3D: startingTransform2)
        
        let group = CAAnimationGroup()
        group.animations = [tA, tA2]
        group.duration = duration1 + duration2
        group.isRemovedOnCompletion = false
        group.fillMode = kCAFillModeForwards
        
        layer.add(group, forKey: "scale")
    }
    func layerScaleAnimationBg(layer: CALayer) {
        let timing = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        let tA = CABasicAnimation(keyPath: "transform")
        tA.timingFunction = timing
        tA.duration = 0.5
        tA.isRemovedOnCompletion = false
        tA.fillMode = kCAFillModeForwards
        let startingTransform = CATransform3DMakeScale(1.3, 1.3, 1)
        tA.toValue = NSValue(caTransform3D: startingTransform)
        
        let tA2 = CABasicAnimation(keyPath: "transform")
        tA2.timingFunction = timing
        tA2.duration = 1.1
        tA2.beginTime = 0.5
        tA2.isRemovedOnCompletion = false
        tA2.fillMode = kCAFillModeForwards
        let startingTransform2 = CATransform3DMakeScale(0.8, 0.8, 1)
        tA2.toValue = NSValue(caTransform3D: startingTransform2)
        
        let group = CAAnimationGroup()
        group.animations = [tA, tA2]
        group.duration = 1.6
        group.isRemovedOnCompletion = false
        group.fillMode = kCAFillModeForwards
        
        layer.add(group, forKey: "scale")
    }
    
}
