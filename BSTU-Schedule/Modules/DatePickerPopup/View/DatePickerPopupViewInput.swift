protocol DatePickerPopupViewInput: class {

    func setupInitialState()
    func reloadPickerView()
}
