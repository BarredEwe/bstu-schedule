import Foundation

class PersonalAreaPresenter: PersonalAreaModuleInput, PersonalAreaViewOutput, PersonalAreaInteractorOutput {

    weak var view: PersonalAreaViewInput!
    var interactor: PersonalAreaInteractorInput!
    var router: PersonalAreaRouterInput!

    func viewIsReady() {
        registerNotification()
    }
    
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateScreenAfterAuthorization), name: NSNotification.Name(rawValue: "UserAction"), object: nil)
    }
    
    @objc func updateScreenAfterAuthorization() {
        self.view.updateScreenAfterAuthorization()
    }
    
    func navigateToCreateEvent() {
        self.router.navigateToCreateEvent()
    }
    
    func userLogout() {
        self.router.logoutAlert { [unowned self] in
            self.interactor.logoutUser()
            self.view.updateScreenAfterAuthorization()
        }
    }
    
    func navigateToLogin() {
        self.router.navigateToLogin()
    }
    
    func clearChache() {
        self.router.clearChahceAlert { [unowned self] in
            self.interactor.clearChache()
        }
    }
}
