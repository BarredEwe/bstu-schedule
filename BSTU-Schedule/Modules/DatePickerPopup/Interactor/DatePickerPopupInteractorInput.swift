import Foundation

protocol DatePickerPopupInteractorInput {
    
    func sendResultWith(index: Int)
    func setDelegate(delegate: SearchResultDelegate)
    func getAllClassTime() -> [ClassTime]
    func loadAllClassTime()

}
