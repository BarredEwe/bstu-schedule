import UIKit
import RealmSwift

class DayDB: DBManager {
    
    func removeDeletedObjects(IDs: [String]) {
        for id in IDs {
            self.removeObjects(key: id)
        }
    }
    
    func clearChacheDays() {
        self.removeAllObjectOfType(type: Event.self)
        self.removeAllObjectOfType(type: Lesson.self)
        self.removeAllObjectOfType(type: Room.self)
        self.removeAllObjectOfType(type: Group.self)
        self.removeAllObjectOfType(type: Disciple.self)
    }
    
    func getObjects(filter: Filter) -> [Day]? {
        let events = EventDB().getObjects(filter: filter)
        
        if events?.count == 0 {
            return nil
        }
        
        let daysInWeek = 6
        
        var days = [Day]()
        
        for _ in 0...daysInWeek {
            let day = Day()
            day.date = filter.day
            days.append(day)
        }
        
        for event in events! {
            days[event.day].events.append(event)
        }
        
        for day in days {
            if day.events.count == 0 {
                days.remove(at: days.index(of: day)!)
            }
        }
        
        return days
    }
    
    func fetchObjects(days: [Day]) {
        var events = [Event]()
        for day in days {
            for event in day.events {
                events.append(event)
            }
        }
        EventDB().fetchObjects(events: events)
    }
    
    func removeOldObjects() {
        EventDB().removeOldObject()
    }
    
}
