import UIKit

protocol DatePickerPopupViewOutput {
    
    func viewIsReady()
    func sendResultWith(index: Int)
    func loadAllClassTime()
    func getAllCassTime() -> [ClassTime]
    func navigateBack()
}
