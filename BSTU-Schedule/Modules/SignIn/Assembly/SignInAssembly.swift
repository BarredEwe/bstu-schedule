import UIKit

class SignInAssembly {
    
    class func assembly() -> UIViewController {
        let storyBoard = UIStoryboard(name: "SignInView", bundle: nil)
        let viewController = storyBoard.instantiateInitialViewController() as! SignInViewController
        
        let router = SignInRouter()
        router.view = viewController
        
        let presenter = SignInPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = SignInInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }
    
}
