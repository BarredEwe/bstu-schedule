import UIKit

class EventMapper {
    
    class func eventFromRespocne(responseObject: AnyObject) -> (Event?, [String: AnyObject]?)? {
        
        let event = Event()
        var changeDatas: [String: AnyObject]?
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let item = responseObject as? [String: AnyObject] else {
            return nil
        }
        
        event.id = item["id"] as! String
        
        let date = item["date"] as! [String: AnyObject]
        event.day = date["day"] as! Int
        
        let drop = date["drop"] as? Int
        if drop != nil {
            event.drop = drop!
        }
        
        var dates = date["academicPeriod"] as? [String: AnyObject]
        if dates != nil {
            dates = dates?["date"] as? [String: AnyObject]
            event.startDate = dateFormatter.date(from: dates?["start"] as! String)
            event.endDate = dateFormatter.date(from: dates?["end"] as! String)
        } else {
            let date = dateFormatter.date(from: date["date"] as! String)!
            let firstdate = date.startOfWeek!
            event.startDate = firstdate
            event.endDate = Calendar.current.date(byAdding: .day, value: 7, to: firstdate)!
        }
        
        let classTime = item["classTime"] as! [String: AnyObject]
        let time = (classTime["time"] as! [String: AnyObject])
        event.startTime = timeFormatter.date(from: time["start"] as! String)
        event.endTime = timeFormatter.date(from: time["end"] as! String)
        
        let type = (item["eventType"] as! [String: AnyObject])
        let eventType = LessonType()
        eventType.name = type["name"] as! String
        eventType.id = type["id"] as! String
        event.type = eventType
        
        let groups = GroupMapper.objectsFromResponce(responseObject: item as AnyObject, type: "groups")
        for group in groups! {
            event.groups.append(GroupMapper.objectFrom(object: group))
        }
        
        let lectureHall = RoomMapper.objectFromRespocne(responseObject: item["lectureHall"] as! [String: AnyObject])
        event.audience = RoomMapper.objectFrom(object: lectureHall)
        
        let disciples = DiscipleMapper.objectsFromResponce(responseObject: item as AnyObject, type: "lecturer")
        for disciple in disciples! {
            event.teachers.append(DiscipleMapper.objectFrom(object: disciple))
        }
        
        let lesson = LessonMapper.objectFromRespocne(responseObject: item["subject"] as! [String: AnyObject])
        event.lesson = LessonMapper.objectFrom(object: lesson)
        
        //ChangeDataEvent
        if let changeData = item["changeData"] as? [String: AnyObject] {
            changeDatas = changeData
        }
        
        return (event, changeDatas)
    }
    
    
}
