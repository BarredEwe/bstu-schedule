import UIKit

class DatePickerPopupRouter: DatePickerPopupRouterInput {
    
    weak var view: UIViewController?
    
    func navigateBack() {
        view?.dismiss(animated: true, completion: nil)
    }

}
