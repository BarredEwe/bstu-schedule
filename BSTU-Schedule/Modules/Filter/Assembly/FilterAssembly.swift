import UIKit

class FilterAssembly {
    
    class func assembly() -> UIViewController {
        let storyBoard = UIStoryboard(name: "FilterView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! FilterViewController
        
        let router = FilterRouter()
        router.view = vc
        
        let presenter = FilterPresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = FilterInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
