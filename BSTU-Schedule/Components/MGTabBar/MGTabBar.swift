import UIKit

class MGTabBar: UITabBarController, UITabBarControllerDelegate {
    
    var tabBarItemOne = UITabBarItem()
    var filter = Filter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.white
        tabBar.barTintColor = UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(userActions), name: NSNotification.Name(rawValue: "UserAction"), object: nil)
        setupPages()
        
        if APIUser.chechAuthorize() {
            self.selectedIndex = 0
        } else {
            self.selectedIndex = 1
        }
    }
    
    func setupPages() {
        let tabOne = FilterAssembly.assembly()
        let tabOneBarItem = UITabBarItem(title: "Поиск", image: UIImage(named: "Search"), selectedImage: UIImage(named: "SearchFill"))
        let root = self.createNavControllerWith(viewController: tabOne)
        root.tabBarItem = tabOneBarItem
        
        // Create Tab two
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if !APIUser.chechAuthorize() {
            filter.day = Date().startOfWeek!
            filter.data = "13-ИВТ1"
            filter.type = .teacher
        } else {
            let user = UserDB().getUser()!
            filter = UserMapper.getFilterFrom(user: user)
        }
        
        let tabTwo = ScheduleContainerAssembly.assemblyWith(filter: filter)
        let tabBarItemOne = UITabBarItem(title: "Главная", image: UIImage(named: "Home"), selectedImage: UIImage(named: "HomeFill"))
        let root1 = self.createNavControllerWith(viewController: tabTwo)
        root1.tabBarItem = tabBarItemOne
        
        // Create Tab three
        let tabThree = PersonalAreaAssembly.assembly()
        let tabThreeBarItem3 = UITabBarItem(title: "Личный кабинет", image: UIImage(named: "User"), selectedImage: UIImage(named: "UserFill"))
        let root2 = self.createNavControllerWith(viewController: tabThree)
        root2.tabBarItem = tabThreeBarItem3
        
        self.viewControllers = [root1, root, root2]
        removeTabbarItemsText()
        userActions()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let animation = CATransition()
        animation.type = kCATransitionFade
        animation.duration = 0.1
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        self.view.window?.layer.add(animation, forKey: "fadeTransition")
    }
    
    func createNavControllerWith(viewController: UIViewController) -> UINavigationController {
        let root = UINavigationController(rootViewController: viewController)
        root.navigationBar.isTranslucent = false
        root.navigationBar.barTintColor = UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
        root.navigationBar.barStyle = .black
        root.navigationBar.tintColor = UIColor.white
        
        if let barFont = UIFont(name: ".SFUIText-Medium", size: 20.0) {
            root.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: barFont]
        }
        return root
    }
    
    func removeTabbarItemsText() {
        if let items = tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
            }
        }
    }
    
    @objc func userActions() {
        if APIUser.chechAuthorize() {
            self.tabBar.items?[0].isEnabled = true
            
        } else {
            self.tabBar.items?[0].isEnabled = false
        }
    }
}
