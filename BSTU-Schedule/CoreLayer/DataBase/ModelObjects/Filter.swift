import Foundation

enum TypeUser: String {
    case teacher = "teacher"    //Ученик
    case disciple = "disciple"  //Учитель
}

class Filter {
    var type: TypeUser = .disciple
    var data = ""
    var day: Date?
}
