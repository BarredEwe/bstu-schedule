import Foundation

protocol CalendarRouterInput {
    
    func navigateToShowDayScheduleWith(filter: Filter)
    func navigateBack()
    
}
