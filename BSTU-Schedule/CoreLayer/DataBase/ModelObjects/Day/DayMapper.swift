import UIKit

class DayMapper {
    
    class func managedDayToUnmanaged(days: [Day]) -> [Day] {
        var newDays = [Day]()
        
        for day in days {
            let newDay = Day()
            newDay.date = day.date
            for event in day.events {
                newDay.events.append(Event(value: event))
            }
            newDays.append(Day(value: newDay))
        }
        return newDays
    }
    
    class func daysToEvents(days: [Day]?) -> [Event]? {
        var events = [Event]()
        
        guard (days != nil) else {
            return nil
        }
        
        for day in days! {
            for event in day.events {
                events.append(event)
            }
        }
        return events
    }
    
    class func sortedDays(days: [Day]) {
        for day in days {
            let events = day.events.sorted( by: {$0.startTime! < $1.startTime! })
            day.events.removeAll()
            for event in events {
                day.events.append(event)
            }
        }
    }
    
    class func deleteChangedEventsFrom(days: [Day]) {
        for day in days {
            for event in day.events {
                if event.idEvent != "" {
                    let eventRm = day.events.filter {
                        ($0.id.contains(event.idEvent) && $0.startDate! <= event.startDate! && $0.endDate! >= event.endDate!)
                    }
                    if eventRm.first != nil {
                        if eventRm.first!.idEvent == "" {
                            let index = day.events.index(of: eventRm.first!)
                            day.events.remove(at: index!)
                        }
                    }
                }
            }
        }
    }
    
    class func removeDeletedEventsFrom(days: [Day]?, daysFromDB: [Day]?) {
        
        guard (daysFromDB != nil) else {
            return
        }
        
        var idDeletes = [String]()
        
        let eventsDB = DayMapper.daysToEvents(days: daysFromDB)
        let events = DayMapper.daysToEvents(days: days)
        
        for event in eventsDB! {
            let eventRm = events?.filter{
                ($0.id == event.id)
            }
            if eventRm?.first == nil {
                if !idDeletes.contains(event.id) {
                    idDeletes.append(event.id)
                    print(event)
                }
            }
        }
        
        if idDeletes.count > 0 {
            DayDB().removeDeletedObjects(IDs: idDeletes)
        }
        
    }
    
    class func loadEventsFrom(responce: AnyObject, date: Date) -> [Event] {
        
        var events = [Event]()
        
        for item in responce as! [[String: AnyObject]] {
            let eventISChanges = EventMapper.eventFromRespocne(responseObject: item as AnyObject)
            let event = eventISChanges?.0
            
            let drop = date.evenOddWeek()
            if event?.drop == drop || event?.drop == 0 {
                if let responce = eventISChanges?.1 {
                    if let eventChange = eventChanges(event: event!, changeData: responce) {
                        events.append(eventChange)
                    }
                    if responce["changeType"] as! Int != 1 {
                        events.append(event!)
                    }
                } else {
                    events.append(event!)
                }
            }
        }

        return events
    }
    
    class func dayFromRespocne(responseObject: AnyObject, filter: Filter) -> Day? {
        let object = Day()
        var events = [Event]()
        
        if let items = responseObject["basic"] as? [[String: AnyObject]] {
            for event in loadEventsFrom(responce: items as AnyObject, date: filter.day!) {
                events.append(event)
            }
        }
        
        if let items = responseObject["other"] as? [[String: AnyObject]] {
            for event in loadEventsFrom(responce: items as AnyObject, date: filter.day!) {
                event.single = true
                events.append(event)
            }
        }
        
        for event in events {
            object.events.append(event)
        }
        
        return object
    }
    
    class func eventChanges(event: Event, changeData: [String: AnyObject]) -> Event? {
        let changeDataEvent = Event()
        
        let audience = Room()
        audience.name = (event.audience?.name)!
        audience.id = (event.audience?.id)!
        changeDataEvent.audience = audience
        
        changeDataEvent.day = event.day
        changeDataEvent.drop = event.drop
        
        let group = Group()
        group.name = (event.groups.first?.name)!
        group.id = (event.groups.first?.id)!
        changeDataEvent.groups.append(group)
        
        let lesson = Lesson()
        lesson.name = (event.lesson?.name)!
        lesson.id = (event.lesson?.id)!
        changeDataEvent.lesson = lesson
        
        changeDataEvent.type = event.type
        changeDataEvent.startTime = event.startTime
        changeDataEvent.endTime = event.endTime
        
        let teacher = Disciple()
        teacher.name = (event.teachers.first?.name)!
        teacher.id = (event.teachers.first?.id)!
        changeDataEvent.teachers.append(teacher)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        changeDataEvent.changedInfo = changeData["changeText"] as! String
        
        let startDate = dateFormatter.date(from: changeData["changeDate"] as! String)
        let newStartDate = startDate?.startOfWeek
        changeDataEvent.startDate = newStartDate
        let newdate = Calendar.current.date(byAdding: .day, value: 6, to: newStartDate!)!
        changeDataEvent.endDate = newdate
        
        if changeDataEvent.changedInfo == "Событие перенесено!" {
            changeDataEvent.idEvent = changeData["changeId"] as! String
            changeDataEvent.id = event.id
        } else {
            changeDataEvent.idEvent = event.id
            changeDataEvent.id = changeData["changeId"] as! String
        }
        
        return changeDataEvent
    }
    
    class func daysFromResponce(responseObject: AnyObject, filter: Filter) -> [Day]? {
        
        var objects = [Day]()
        
        guard let items = responseObject["timetable"] as? [[String: AnyObject]] else {
            return nil
        }
        
        for item in items {
            let day = dayFromRespocne(responseObject: item as AnyObject, filter: filter)
            day?.date = filter.day
            
            if day?.events.count != 0 {
                objects.append(day!)
            }
        }
        
        return objects
    }
    
}
