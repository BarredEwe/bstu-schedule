import UIKit

class ScheduleAssembly {
    
    class func assemblyWith(filter: Filter) -> UIViewController {
        let storyBoard = UIStoryboard(name: "ScheduleView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! ScheduleViewController
        
        let router = ScheduleRouter()
        router.view = vc
        
        let presenter = SchedulePresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = ScheduleInteractor()
        interactor.setFilter(filter: filter)
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
