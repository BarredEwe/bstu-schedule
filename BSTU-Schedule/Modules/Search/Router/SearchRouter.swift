import UIKit

class SearchRouter: SearchRouterInput {
    
    weak var view: UIViewController?
    
    func navigateBack() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionFade
        view?.navigationController?.view.layer.add(transition, forKey: kCATransition)
        view?.navigationController?.popViewController(animated: false)
    }
}
