import XCTest
import RealmSwift
@testable import BSTU_Schedule

class MGObjectDBTests: XCTestCase {

    override func setUp() {
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFetchObjects() {
        
        let testRealm = try! Realm()
        
        let object = Disciple()
        object.name = "Test Disciple"
        object.id = "123456789"

        MGObjectDB().fetchObjects(groups: [object])
        XCTAssertEqual(testRealm.objects(Disciple.self).first!, object,
                       "Disciple was not fetched.")
    }
    
    func testGetObjects() {
        let testRealm = try! Realm()
        
        let object = Disciple()
        object.name = "Test Disciple"
        object.id = "123456789"
        
        MGObjectDB().fetchObjects(groups: [object])
        
        let objects = MGObjectDB().getObjects(filter: "Test Disciple", type: Disciple())
        XCTAssertEqual(testRealm.objects(Disciple.self).first!, objects?.first,
                       "Disciple get is not work.")
    }
    
}
