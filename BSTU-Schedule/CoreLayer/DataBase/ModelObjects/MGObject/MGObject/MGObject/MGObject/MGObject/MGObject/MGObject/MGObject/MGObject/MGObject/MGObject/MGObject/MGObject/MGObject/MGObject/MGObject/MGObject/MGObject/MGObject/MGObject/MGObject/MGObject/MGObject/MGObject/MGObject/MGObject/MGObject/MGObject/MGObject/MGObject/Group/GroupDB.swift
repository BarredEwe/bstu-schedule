import RealmSwift

class GroupDB: MGObjectDB {
    
    func fetchGroups<T: Object>(groups: [T]) {
        super.fetchObjects(groups: groups)
    }
    
    func getGroups(filter: String) -> [Group]? {
        return super.getObjects(filter: filter, type: Group())
    }
    
}
