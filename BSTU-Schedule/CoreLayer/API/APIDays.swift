import UIKit
import RealmSwift

/**
 Type of data received.
 
 - DataBase: Data received from the database.
 - Server: Data received from the server
 */
enum typeReceived {
    case DataBase
    case Server
}

let urlForGetDays = "event/week/"
let searchTypeGroup = "group"
let searchTypeLecture = "lecture"

/// Allows you to load schedule days from the server
class APIDays: APIClient, APIDaysProtocol {
    
    class func getDaysWith(filter: Filter, success: @escaping ([Day]?, typeReceived) -> (), failure: @escaping (NSError) -> () ) {
        
        var type = searchTypeGroup
        if filter.type == .disciple {
            type = searchTypeLecture
        }
        
        let dF = DateFormatter()
        dF.dateFormat = "yyyy-MM-dd"
        let time = dF.string(from: filter.day!)
        
        let URL = urlForGetDays + time + "/" + type + "/" + filter.data
        
        let daysFromDB = DayDB().getObjects(filter: filter)
        
        if daysFromDB != nil {
            DayMapper.deleteChangedEventsFrom(days: daysFromDB!)
            DayMapper.sortedDays(days: daysFromDB!)
            let unmanagedDaysDB = DayMapper.managedDayToUnmanaged(days: daysFromDB!)
            success(unmanagedDaysDB, .DataBase)
        } else {
            success(nil, .DataBase)
        }
        
        self.requset(methodType: .get, URL: URL, headers: nil, parametres: nil, success: { object in
            let days = DayMapper.daysFromResponce(responseObject: object, filter: filter)
            
            if days != nil {
                DayDB().fetchObjects(days: days!)
                let unmanagedDays = DayMapper.managedDayToUnmanaged(days: days!)
                DayMapper.sortedDays(days: unmanagedDays)
                DayMapper.removeDeletedEventsFrom(days: unmanagedDays, daysFromDB: daysFromDB)
                DayMapper.deleteChangedEventsFrom(days: unmanagedDays)
                success(unmanagedDays, .Server)
            } else {
                success(nil, .Server)
            }
        }) { error in
            failure(error)
        }
        
    }
    
}
