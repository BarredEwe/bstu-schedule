//
//  DatePickerPopupDatePickerPopupRouterInput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 04/06/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

import Foundation

protocol DatePickerPopupRouterInput {
    func navigateBack()
}
