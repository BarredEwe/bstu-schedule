import Foundation

protocol ScheduleInteractorInput {
    
    func setFilter(filter: Filter)
    func getDays() -> [Day]
    func loadDay()
    func getFilter() -> Filter
    
}
