import UIKit

class PersonalAreaRouter: PersonalAreaRouterInput {
    
    var view: UIViewController?
    
    func navigateToCreateEvent() {
        let workWithEventView = WorkWithEventAssembly.assembly()
        self.view?.navigationController?.pushViewController(workWithEventView, animated: true)
    }
    
    func navigateToLogin() {
        let vc = SignInAssembly.assembly()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        view?.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func logoutAlert(actionOk: @escaping ()->()) {
        let alertView = UIAlertController(title: "Выход",
                                          message: "Вы действительно хотите выйти?" as String, preferredStyle:.alert)
        let okAction = UIAlertAction(title: "Да", style: .default, handler: { action in
            actionOk()
        })
        alertView.addAction(okAction)
        
        let noAction = UIAlertAction(title: "Нет", style: .cancel, handler: nil)
        alertView.addAction(noAction)
        
        view?.present(alertView, animated: true, completion: nil)
    }
    
    func clearChahceAlert(actionOk: @escaping ()->()) {
        let alertView = UIAlertController(title: "Подтвердите",
                                          message: "Очистить кеш?", preferredStyle:.alert)
        
        let yesAction = UIAlertAction(title: "Да", style: .destructive, handler: { action in
            actionOk()
        })
        alertView.addAction(yesAction)
        
        let noAction = UIAlertAction(title: "Нет", style: .cancel, handler: nil)
        alertView.addAction(noAction)
        
        view?.present(alertView, animated: true, completion: nil)

    }
    
}
