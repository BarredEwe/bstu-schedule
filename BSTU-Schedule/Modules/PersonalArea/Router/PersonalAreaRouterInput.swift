//
//  PersonalAreaPersonalAreaRouterInput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 12/04/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

import Foundation

protocol PersonalAreaRouterInput {
    
    func navigateToCreateEvent()
    func logoutAlert(actionOk: @escaping ()->())
    func navigateToLogin()
    func clearChahceAlert(actionOk: @escaping ()->())

}
