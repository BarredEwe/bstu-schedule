import UIKit
import Alamofire

protocol APIClientProtocol {
    /**
     Sends a request to the server.
     
     - Parameter methodType: The event you want to create.
     - Parameter URL: Additional link.
     - Parameter baseURL: Base link to the server.
     - Parameter headers: HTTP Headers.
     - Parameter parametres: Additional parametres.
     - Parameter success: Successful requset.
     - Parameter failure: A failed requset.
     */
    static func requset(methodType: HTTPMethod, URL: String, baseURL: String, headers: Dictionary <String, String>?, parametres: Dictionary <String, Any>?, success: @escaping (AnyObject) -> Void, failure: @escaping (NSError) -> Void)
    
}
