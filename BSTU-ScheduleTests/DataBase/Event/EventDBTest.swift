import XCTest
import RealmSwift
@testable import BSTU_Schedule

class EventDBTests: XCTestCase {
    
    let dateFormatter = DateFormatter()
    
    override func setUp() {
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        dateFormatter.dateFormat = "yyyy-MM-dd"
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFetchEvents() {
        let testRealm = try! Realm()
        
        let event = Event()
        event.id = "123456"
        
        EventDB().fetchObjects(events: [event])
        
        XCTAssertEqual(testRealm.objects(Event.self).first!, event,
                       "Event was not fetched.")
    }
    
    func getEventForTest() -> Event {
        let event = Event()
        event.id = "1"
        event.day = 0
        event.drop = 0
        
        let group = Group()
        group.name = "Test"
        group.id = "2"
        event.groups.append(group)
        
        let disciple = Disciple()
        disciple.name = "Test"
        disciple.id = "3"
        event.teachers.append(disciple)
        
        event.startDate = dateFormatter.date(from: "2017-04-10")
        event.endDate = dateFormatter.date(from: "2017-04-16")
        
        return event
    }
    
    func getFilterForTest() -> Filter {
        let filter = Filter()
        filter.type = .teacher
        filter.data = "Test"
        filter.day = dateFormatter.date(from: "2017-04-13")
        
        return filter
    }
    
    func testGetEvents() {
        let event = getEventForTest()
        let filter = getFilterForTest()
        
        EventDB().fetchObjects(events: [event])
        
        let objectOfTeacher = EventDB().getObjects(filter: filter)?.first
        XCTAssertEqual(event, objectOfTeacher,
                       "Error get event.")
        
        filter.type = .disciple
        filter.data = "Test"
        
        let objectOfDisciple = EventDB().getObjects(filter: filter)?.first
        XCTAssertEqual(event, objectOfDisciple,
                       "Error get event.")
    }
    
    func testRemoveOldEvents() {
        let testRealm = try! Realm()
        let event = getEventForTest()
        
        EventDB().fetchObjects(events: [event])
        EventDB().removeOldObject()
        
        XCTAssertNil(testRealm.objects(Event.self).first, "Error remove old event.")
    }
    
}
