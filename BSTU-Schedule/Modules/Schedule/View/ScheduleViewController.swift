import UIKit

class ScheduleViewController: UIViewController, ScheduleViewInput, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var output: ScheduleViewOutput!
    
    var refreshControll: UIRefreshControl!

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var notEventsLabel: UILabel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRefreschControll()
        refreshControll.beginRefreshing()
        beginUpdateSchedule()
        
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        beginUpdateSchedule()
    }
    
    deinit {
        print("Deinit schedule")
    }


    // MARK: ScheduleViewInput
    func setupInitialState() {
        
    }
    
    func setupRefreschControll() {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(beginUpdateSchedule), for: .valueChanged)
        refreshControll = refresher
        collectionView!.addSubview(refresher)
    }
    
    @objc func beginUpdateSchedule() {
        self.output.loadDay()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.output.days().count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.output.days()[section].events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DataCell", for: indexPath) as! DataCell
        if indexPath.row == self.output.days()[indexPath.section].events.count - 1 {
            cell.backView.layer.masksToBounds = true
            cell.backView.layer.cornerRadius = 8
            cell.separatorView.isHidden = true
        } else {
            cell.backView.layer.cornerRadius = 0
            cell.separatorView.isHidden = false
        }
        
        cell.configure(event: self.output.days()[indexPath.section].events[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var cell: CustomScheduleHeader?
        if kind == UICollectionElementKindSectionHeader {
            cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCell", for: indexPath) as? CustomScheduleHeader
            cell?.backView.layer.cornerRadius = 8
            
            let day = self.output.days()[indexPath.section]
            cell?.configure(day: day, week: (day.events.first?.day)!)
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.output.didSelectEventAt(index: indexPath.item, section: indexPath.section)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 82)
    }
    
    func updateSchedule() {
        if self.output.days().count == 0 {
            notEventsLabel.isHidden = false
        } else {
            notEventsLabel.isHidden = true
        }
        self.collectionView.reloadData()
        refreshControll.endRefreshing()
    }
}
