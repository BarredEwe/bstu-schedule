import UIKit

class FilterRouter: FilterRouterInput {
    
    weak var view: UIViewController?
    
    func navigateToCalendarWith(filter: Filter, delegate: SearchResultDelegate) {
        let vc = CalendarAssembly.assemblyWith(filter: filter, delegate: delegate)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        view?.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func searchWith(type: TypeSearch, delegate: SearchResultDelegate) {
        let searchView = SearchAssembly.assemblyWith(type: type, delegate: delegate)
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionFade
        
        self.view?.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.view?.navigationController?.pushViewController(searchView, animated: false)
    }
    
    func navigateToShowDayScheduleWith(filter: Filter) {
        let scheduleView = ScheduleContainerAssembly.assemblyWith(filter: filter)
        self.view?.navigationController?.pushViewController(scheduleView, animated: true)
    }
    
}
