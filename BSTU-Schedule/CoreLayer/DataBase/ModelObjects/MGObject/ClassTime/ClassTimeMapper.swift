import UIKit

protocol ClassTimeMapperProtocol {
    //static func objectFrom(object: MGObject) -> LessonType
}

class ClassTimeMapper: ClassTimeMapperProtocol {
    
    class func objectFrom(object: AnyObject) -> ClassTime {
        let classTime = ClassTime()
        classTime.id = object["id"] as! String
        classTime.name = object["name"] as! String
        
        let time = object["time"] as! [String: String]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let start = formatter.date(from: time["start"]!)
        let end = formatter.date(from: time["end"]!)

        classTime.start = start
        classTime.end = end
        return classTime
    }
    
    class func objectsFrom(objects: AnyObject) -> [ClassTime]? {
        
        var classTimes = [ClassTime]()
        
        guard let items = objects["data"] as? [[String: AnyObject]] else {
            return nil
        }
        
        for clTime in items {
            classTimes.append(objectFrom(object: clTime as AnyObject))
        }
        
        return classTimes
    }
    
}
