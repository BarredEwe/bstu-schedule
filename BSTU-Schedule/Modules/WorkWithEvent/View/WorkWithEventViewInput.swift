protocol WorkWithEventViewInput: class {

    /**
        @author Maxim
        Setup initial state of the view
    */

    func setupInitialState()
}
