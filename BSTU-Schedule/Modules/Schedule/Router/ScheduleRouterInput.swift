import Foundation

protocol ScheduleRouterInput {
    
    func navigateToExtendedScheduleWith(event: Event, filter: Filter)

}
