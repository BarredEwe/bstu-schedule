import UIKit

class FilterInteractor: FilterInteractorInput {

    weak var output: FilterInteractorOutput!
    
    var filter: Filter!
    
    init() {
        filter = Filter()
        filter.type = .disciple
    }
    
    func setFilter(type: TypeUser) {
        filter.type = type
    }
    
    func setFilter(data: String) {
        filter.data = data
    }
    
    func setFilter(day: Date) {
        self.filter.day = day
    }
    
    func getFilter() -> Filter {
        return filter
    }
}
