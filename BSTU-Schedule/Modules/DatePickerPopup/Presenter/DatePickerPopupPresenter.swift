import UIKit

class DatePickerPopupPresenter: DatePickerPopupModuleInput, DatePickerPopupViewOutput, DatePickerPopupInteractorOutput {

    weak var view: DatePickerPopupViewInput!
    var interactor: DatePickerPopupInteractorInput!
    var router: DatePickerPopupRouterInput!

    func viewIsReady() {
        
    }
    
    func loadAllClassTime() {
        self.interactor.loadAllClassTime()
    }
    
    func sendResultWith(index: Int) {
        self.interactor.sendResultWith(index: index)
    }
    
    func reloadData() {
        self.view.reloadPickerView()
    }
    
    func getAllCassTime() -> [ClassTime] {
        return self.interactor.getAllClassTime()
    }
    
    func navigateBack() {
        self.router.navigateBack()
    }
}
