import JTAppleCalendar

class CellView: JTAppleCell {
    
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var selectedView: UIImageView!
    @IBOutlet var currentDay: UIImageView!
    
}
