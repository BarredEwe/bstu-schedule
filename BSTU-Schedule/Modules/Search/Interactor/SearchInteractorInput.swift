import UIKit

protocol SearchInteractorInput {
    
    func setType(type: TypeSearch)
    func getTitle() -> String
    func setDelegate(delegate: SearchResultDelegate)
    func sendResult(info: Any)
    func searchWith(info: String)
    func getObjects() -> [MGObject]
    func getLastSearch() -> String

}
