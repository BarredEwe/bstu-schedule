import UIKit

class SearchViewController: UIViewController, SearchViewInput, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    var output: SearchViewOutput!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seacrhIndicator: UIActivityIndicatorView!
    lazy var searchBar = UISearchBar()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar(appear: true)
        self.setupSearchBar()
        output.viewIsReady()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }

    // MARK: SearchViewInput
    func setupInitialState() {
        
    }
    
    func setupNavigationBar(appear: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: false)
        if appear {
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 197/255, green: 77/255, blue: 57/255, alpha: 1.0)
        } else {
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
        }
    }
    
    func setupSearchBar() {
        searchBar.showsCancelButton = false
        self.navigationItem.titleView = searchBar
        searchBar.delegate = self
        searchBar.autocapitalizationType = .none
        searchBar.showsCancelButton = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.output.getObjects().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cellS = cell as! SearchCell
        cellS.name.text = self.output.getObjects()[indexPath.row].name
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.setupNavigationBar(appear: false)
        self.view.endEditing(true)
        self.output.finisSearch(info: self.output.getObjects()[indexPath.row].name)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        seacrhIndicator.startAnimating()
        self.output.searchWith(info: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.setupNavigationBar(appear: false)
        self.view.endEditing(true)
        self.output.navigateBack()
    }
    
    func updateInfo() {
        seacrhIndicator.stopAnimating()
        self.tableView.reloadData()
    }
}
