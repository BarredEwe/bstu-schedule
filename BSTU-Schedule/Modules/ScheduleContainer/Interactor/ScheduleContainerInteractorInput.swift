import UIKit

protocol ScheduleContainerInteractorInput {

    func getFilter() -> Filter
    func createPages()
    func setFilter(filter: Filter)
    func setFilter(day: Date)
    func getPageAfter(viewController: UIViewController) -> UIViewController?
    func getPageBefore(viewController: UIViewController) -> UIViewController?
    func getPages() -> [UIViewController]
}
