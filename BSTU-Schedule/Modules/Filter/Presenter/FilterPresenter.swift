import UIKit

class FilterPresenter: FilterModuleInput, FilterViewOutput, FilterInteractorOutput, SearchResultDelegate {

    weak var view: FilterViewInput!
    var interactor: FilterInteractorInput!
    var router: FilterRouterInput!

    func viewIsReady() {
    }
    
    func setFilter(type: TypeUser) {
        self.interactor.setFilter(type: type)
    }
    
    func setFilter(data: String) {
        self.interactor.setFilter(data: data)
    }
    
    func getFilter() -> Filter {
        return self.interactor.getFilter()
    }
    
    func nextButtonDidPressed() {
        let filter = self.interactor.getFilter()
        self.router.navigateToShowDayScheduleWith(filter: filter)
    }
    
    func searchWith(type: TypeSearch) {
        self.router.searchWith(type: type, delegate: self)
    }
    
    func searchDate() {
        self.router.navigateToCalendarWith(filter: self.getFilter(), delegate: self)
    }
    
    //MARK: SearchResultDelegate
    func didFinisedSearchWith(result: Any) {
        if result is Date {
            self.interactor.setFilter(day: result as! Date)
            self.view.updateDate()
        } else {
            self.setFilter(data: result as! String)
            self.view.updateName()
        }
    }

}
