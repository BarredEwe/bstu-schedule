import UIKit

protocol APIDaysProtocol {
    
    /**
     Loads the days of the schedule from the server.
     
     - Parameter filter: Used to determine the type and date of the schedule.
     - Parameter success: Successful loading of schedule days.
     - Parameter failure: A failed loading of schedule days.
     */
    static func getDaysWith(filter: Filter, success: @escaping ([Day]?, typeReceived) -> (), failure: @escaping (NSError) -> () )
    
}
