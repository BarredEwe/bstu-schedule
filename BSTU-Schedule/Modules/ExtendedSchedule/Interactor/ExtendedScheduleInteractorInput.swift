import Foundation

protocol ExtendedScheduleInteractorInput {
    func setEvent(event: Event)
    func getEvent() -> Event
    func getFilter() -> Filter
    func setFilter(filter: Filter)
}
