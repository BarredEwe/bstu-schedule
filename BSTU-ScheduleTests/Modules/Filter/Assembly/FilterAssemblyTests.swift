import UIKit
import XCTest
@testable import BSTU_Schedule

class FilterAssemblyTests: XCTestCase {
    
    var viewController: FilterViewController!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAssemblyModule() {
        
        viewController = FilterAssembly.assembly() as! FilterViewController
        
        XCTAssertNotNil(viewController.output, "FilterViewController is nil after assembly")
        XCTAssertTrue(viewController.output is FilterPresenter, "output is not FilterPresenter")
        
        let presenter: FilterPresenter = viewController.output as! FilterPresenter
        XCTAssertNotNil(presenter.view, "view in FilterPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in FilterPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is FilterRouter, "router is not FilterhRouter")
        
        let interactor: FilterInteractor = presenter.interactor as! FilterInteractor
        XCTAssertNotNil(interactor.output, "output in FilterInteractor is nil after assembly")
    }
    
}
