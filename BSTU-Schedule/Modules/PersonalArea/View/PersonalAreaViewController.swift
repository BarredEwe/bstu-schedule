import UIKit
import UserNotifications

class PersonalAreaViewController: UIViewController, PersonalAreaViewInput {
    
    var output: PersonalAreaViewOutput!
    
    @IBOutlet weak var personalButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var group: UILabel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateScreenAfterAuthorization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateScreenAfterAuthorization()
    }
    
    // MARK: UpdateScreen
    func updateScreenAfterAuthorization() {
        if !APIUser.chechAuthorize() {
            personalButton.setTitle("Войти", for: .normal)
            group.isHidden = true
            nameLabel.isHidden = true
            personalButton.isHidden = false
        } else {
            let user = UserDB().getUser()
            if user?.type == "Студент" {
                personalButton.isHidden = true
                group.isHidden = false
                
            } else {
                group.isHidden = true
                personalButton.isHidden = false
                personalButton.setTitle("Создать событие", for: .normal)
            }
            nameLabel.isHidden = false
            nameLabel.text = user?.name
        }
        
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        self.view.backgroundColor = ColorScheme.mainBackgroundColor()
        if APIUser.chechAuthorize() {
            let rightButton = UIBarButtonItem(image: UIImage(named: "Settings"), style: .plain, target: self, action: #selector(touchSettingBarButton))
            let leftButton = UIBarButtonItem(image: UIImage(named: "Exit"), style: .plain, target: self, action: #selector(touchExitBarButton))
            self.navigationItem.rightBarButtonItem = rightButton
            self.navigationItem.leftBarButtonItem = leftButton
        } else {
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    
    // MARK: PersonalAreaViewInput
    func setupInitialState() {
        
    }
    
    // MARK: Actions
    @objc func touchExitBarButton() {
        self.output.userLogout()
    }
    
    @IBAction func touchCreateEventButton(_ sender: UIButton) {
        if APIUser.chechAuthorize() {
            self.output.navigateToCreateEvent()
        } else {
            self.output.navigateToLogin()
        }
    }
    
    @objc func touchSettingBarButton() {
        self.output.clearChache()
    }
}
