import UIKit

/// A common class for the same simple objects
class APIMGObject: APIClient {
    
    /**
     Obtaining an object from the server.
     
     - Parameter type: Type of received object.
     - Parameter name: Name of received object.
     - Parameter count: Number of received objects.
     - Parameter success: Successful receipt of objects.
     - Parameter failure: A failed receipt of objects.
     */
    class func getMGObjectWith(type: String, name: String, count: String = "all" , success: @escaping (AnyObject) -> (), failure: @escaping (NSError) -> () ) {
        
        var header: Dictionary<String, String>?
        if APIUser.chechAuthorize() {
            header = ["X-Auth-Token": UserDB().getUser()?.token] as?  Dictionary<String, String>
        }
        
        self.requset(methodType: .get, URL: type + "/name/" + name + "/count/" + count, headers: header, parametres: nil, success: { object in
            success(object)
        }) { error in
            failure(error)
        }
    }
    
}
