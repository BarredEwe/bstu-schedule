import UIKit

protocol SearchDelegate {
    func didFinisedSearchWith(date: Date)
}

class SearchAssembly {
    
    class func assemblyWith(type: TypeSearch, delegate: SearchResultDelegate) -> UIViewController {
        let storyBoard = UIStoryboard(name: "SearchView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! SearchViewController
        
        let router = SearchRouter()
        router.view = vc
        
        let presenter = SearchPresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = SearchInteractor()
        interactor.setType(type: type)
        interactor.setDelegate(delegate: delegate)
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
