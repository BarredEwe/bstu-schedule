import Foundation
import RealmSwift

class Event: Object {
    
    @objc dynamic var startTime: Date?
    @objc dynamic var endTime: Date?
    @objc dynamic var startDate: Date?
    @objc dynamic var endDate: Date?
    
    @objc dynamic var type: LessonType?
    @objc dynamic var lesson: Lesson?
    @objc dynamic var audience: Room?
    
    @objc dynamic var changedInfo = ""
    @objc dynamic var idEvent = ""
    @objc dynamic var typeChange = ""
    
    @objc dynamic var drop = 0
    @objc dynamic var day = 0
    @objc dynamic var id = ""
    @objc dynamic var single: Bool = false
    
    var teachers = List<Disciple>()
    var groups = List<Group>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    /*init(startTime: Date, endTime: Date, startDate: Date, endDate: Date, type: LessonType, lesson: Lesson, audience: Room, changedInfo: String, idEvent: String, drop: Int, day: Int, id: String, single: Bool = false, teachers: List<Disciple>, groups: List<Group>) {
        super.init()
        
        self.startTime = startTime
        self.endTime = endTime
        self.startDate = startDate
        self.endDate = endDate
        self.type = type
        self.audience = audience
        self.changedInfo = changedInfo
        self.idEvent = idEvent
        self.drop = drop
        self.day = day
        self.id = id
        self.single = single
        self.teachers = teachers
        self.groups = groups
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        
    }*/
    
}
