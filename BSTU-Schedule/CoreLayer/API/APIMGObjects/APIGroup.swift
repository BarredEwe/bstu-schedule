import UIKit

class APIGroup: APIMGObject {
    
    class func getGroupsWith(name: String, success: @escaping ([MGObject]?) -> ()) {
        
        let objectsFromDB = GroupDB().getGroups(filter: name)
        success(objectsFromDB)
        
        self.getMGObjectWith(type: "group", name: name, success: { responseObject in
            let objects = MGObjectMapper.objectsFromResponce(responseObject: responseObject, type: "data")
            if objects != nil {
                var groups = [Group]()
                for object in objects! {
                    groups.append(GroupMapper.objectFrom(object: object))
                }
                DispatchQueue.global(qos: .userInitiated).async {
                    GroupDB().fetchGroups(groups: groups)
                }
            }
            success(objects)
        }, failure: { error in
            
        })
    }
    
}
