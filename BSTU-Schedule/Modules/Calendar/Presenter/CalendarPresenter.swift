import JTAppleCalendar

class CalendarPresenter: CalendarModuleInput, CalendarViewOutput, CalendarInteractorOutput {

    weak var view: CalendarViewInput!
    var interactor: CalendarInteractorInput!
    var router: CalendarRouterInput!

    func viewIsReady() {
        
    }
    
    func setFilter(filter: Filter) {
        self.interactor.setFilter(filter: filter)
    }
    
    func startDate() -> Date {
        return self.interactor.startDate()
    }
    
    func endDate() -> Date {
        return self.interactor.endDate()
    }
    
    func viewDidSelect(date: Date) {
        if self.interactor.sendResult(date: date) {
            self.router.navigateBack()
        } else {
            let filter = self.interactor.getFilterWith(date: date)
            self.router.navigateToShowDayScheduleWith(filter: filter)
        }
    }
    
    func selectedDates() -> [Date] {
        return self.interactor.selectedDates()
    }
    
    func navigateBack() {
        self.router.navigateBack()
    }
}
