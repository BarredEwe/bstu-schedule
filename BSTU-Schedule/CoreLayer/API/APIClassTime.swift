import Foundation

class APIClassTime: APIClient {
    
    class func getAllClassTime(success: @escaping ([ClassTime]?) -> (), failure: @escaping (NSError) -> () ) {
        let URL = "classTime/count/all"
        
        var header: Dictionary<String, String>?
        if APIUser.chechAuthorize() {
            header = ["X-Auth-Token": UserDB().getUser()?.token] as?  Dictionary<String, String>
        }
        
        self.requset(methodType: .get, URL: URL, headers: header, parametres: nil, success: { respondeObject in
            let objects = ClassTimeMapper.objectsFrom(objects: respondeObject)
            success(objects)
        }) { error in
            failure(error)
            print(error)
        }
    }

}
