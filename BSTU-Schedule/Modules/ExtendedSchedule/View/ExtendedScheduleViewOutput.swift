protocol ExtendedScheduleViewOutput {

    func viewIsReady()
    func getEvent() -> Event
    func getFilter() -> Filter
    func navigationBack()
}
