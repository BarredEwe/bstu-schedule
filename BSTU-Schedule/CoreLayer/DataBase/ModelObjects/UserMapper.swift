import UIKit

class UserMapper {
    
    class func objectFromRespocne(responseObject: AnyObject) -> User? {
        
        let user = User()
        
        guard let object = responseObject["data"] as? [String: AnyObject] else {
            return nil
        }
        user.name = object["name"] as! String
        user.token = object["token"] as! String
        user.type = object["typeUser"] as! String
        
        if user.type == "Студент" {
            let group = object["groups"] as! NSArray
            user.data = group[0] as! String
        }
        
        return user
    }
    
    class func getFilterFrom(user: User) -> Filter {
        let filter = Filter()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if user.type == "Студент" {
            filter.type = .teacher
            filter.data = user.data
        } else {
            filter.type = .disciple
            filter.data = user.name
        }
        
        filter.day = Date()
        
        return filter
    }
    
}
