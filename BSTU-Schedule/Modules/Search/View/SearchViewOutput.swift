import UIKit

protocol SearchViewOutput {

    func viewIsReady()
    func getTitile() -> String
    func finisSearch(info: String)
    func searchWith(info: String)
    func getObjects() -> [MGObject]
    func navigateBack()
}
