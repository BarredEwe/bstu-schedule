import UIKit
import RealmSwift

class EventDB: DBManager {
    
    func fetchObjects(events: [Event]) {
        self.addObjectAsync(object: events, udpate: true)
    }
    
    func removeOldObject() {
        let newdate = Calendar.current.date(byAdding: .month, value: -1, to: Date())!
        
        let predicate = NSPredicate(format: "endDate <= %@", argumentArray: [newdate])
        self.removeObjects(predicate: predicate)
    }
    
    func getObjects(filter: Filter) -> [Event]? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var typeLink = "groups"
        if filter.type == .disciple {
            typeLink = "teachers"
        }
        
        var day = filter.day?.startOfWeek!
        day = Calendar.current.date(byAdding: .day, value: 6, to: day!)!
        
        let drop = filter.day?.evenOddWeek()
        let format = "startDate <= %@ AND endDate >= %@ AND ANY \(typeLink).name = %@ AND (drop = %@ || drop = %@)"
        let predicate = NSPredicate(format: format, argumentArray: [day!, day!, filter.data, drop!, 0])
        
        guard let objects = self.getObjectsOf(type: Event.self, predicate: predicate) else {
            return nil
        }
        
        var events = Array(objects)
        
        for event in events {
            if !Date().checkIsContainOfWeek(date: filter.day!, dateStart: event.startDate!, dateEnd: event.endDate!, day: event.day) {
                events.remove(at: events.index(of: event)!)
            }
        }
        
        return events
    }
    
}
