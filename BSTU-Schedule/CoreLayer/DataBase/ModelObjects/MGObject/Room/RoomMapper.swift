import UIKit

protocol RoomMapperProtocol {
    static func objectFrom(object: MGObject) -> Room
}

class RoomMapper: MGObjectMapper, RoomMapperProtocol {
    
    class func objectFrom(object: MGObject) -> Room {
        let room = Room()
        room.name = object.name
        room.id = object.id
        return room
    }
    
}
