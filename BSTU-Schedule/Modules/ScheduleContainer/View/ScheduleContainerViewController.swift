import UIKit

class ScheduleContainerViewController: UIPageViewController, UIPageViewControllerDataSource, ScheduleContainerViewInput {

    var output: ScheduleContainerViewOutput!
    //var index = 0

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        self.setupNavigationBar()
        self.setupPages()
    }
    
    func setupNavigationBar() {
        self.view.backgroundColor = ColorScheme.mainBackgroundColor()
        let barButton = UIBarButtonItem(image: UIImage(named: "Calendar"), style: .plain, target: self, action: #selector(touchBarButton))
        self.navigationItem.rightBarButtonItem = barButton
    }


    // MARK: ScheduleContainerViewInput
    func setupInitialState() {
        
    }
    
    func setupPages() {
        self.dataSource = self
        self.setViewControllers([self.output.getPages().first!], direction: .forward, animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return self.output.getPageBefore(viewController: viewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return self.output.getPageAfter(viewController: viewController)
    }
    
    @objc func touchBarButton() {
        self.output.showCalendar()
    }
    
    func refreshPageViewController() {
        self.output.createPages()
        self.dataSource = nil
        self.dataSource = self
        self.setViewControllers([self.output.getPages().first!], direction: .forward, animated: true, completion: nil)
    }
    
}
