import UIKit

class SchedulePresenter: ScheduleModuleInput, ScheduleViewOutput, ScheduleInteractorOutput {

    weak var view: ScheduleViewInput!
    var interactor: ScheduleInteractorInput!
    var router: ScheduleRouterInput!

    func viewIsReady() {

    }
    
    func days() -> [Day] {
        return self.interactor.getDays()
    }
    
    func loadDay() {
        self.interactor.loadDay()
    }
    
    func didSelectEventAt(index: Int, section: Int) {
        let event = self.days()[section].events[index]
        let filter = self.interactor.getFilter()
        self.router.navigateToExtendedScheduleWith(event: event, filter: filter)
    }
    
    func updateSchedule() {
        self.view.updateSchedule()
    }
    

}
