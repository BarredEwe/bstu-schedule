import UIKit

class ClassTime {
    
    @objc dynamic var start: Date?
    @objc dynamic var end: Date?
    @objc dynamic var name = ""
    @objc dynamic var id = ""
    
    /*override class func primaryKey() -> String? {
        return "name"
    }*/
    
}
