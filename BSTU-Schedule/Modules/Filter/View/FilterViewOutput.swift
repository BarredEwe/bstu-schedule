import UIKit

protocol FilterViewOutput {

    func viewIsReady()
    
    func setFilter(type: TypeUser)
    func setFilter(data: String)
    func getFilter() -> Filter
    
    func nextButtonDidPressed()
    func searchWith(type: TypeSearch)
    func searchDate()
}
