import UIKit

class PersonalAreaAssembly {
    
    class func assembly() -> UIViewController {
        let storyBoard = UIStoryboard(name: "PersonalAreaView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! PersonalAreaViewController
        
        let router = PersonalAreaRouter()
        router.view = vc
        
        let presenter = PersonalAreaPresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = PersonalAreaInteractor()
        //interactor.setFilter(filter: filter)
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
