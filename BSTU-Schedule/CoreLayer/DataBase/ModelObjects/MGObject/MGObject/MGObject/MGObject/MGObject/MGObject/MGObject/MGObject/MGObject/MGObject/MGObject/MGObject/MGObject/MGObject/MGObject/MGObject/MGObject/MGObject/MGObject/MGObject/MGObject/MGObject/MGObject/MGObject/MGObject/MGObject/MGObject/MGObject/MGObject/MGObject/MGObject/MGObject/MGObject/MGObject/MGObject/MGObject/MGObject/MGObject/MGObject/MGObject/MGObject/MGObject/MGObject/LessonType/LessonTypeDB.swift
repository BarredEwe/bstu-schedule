import RealmSwift

class LessonTypeDB: MGObjectDB {
    
    func fetchDesicples<T: MGObject>(objects: [T]) {
        let newDisciples = DiscipleMapper.objectsFrom(objects: objects)
        super.fetchObjects(groups: newDisciples)
    }
    
    func getDesciples(filter: String) -> [Disciple]? {
        return super.getObjects(filter: filter, type: Disciple())
    }
    
}
