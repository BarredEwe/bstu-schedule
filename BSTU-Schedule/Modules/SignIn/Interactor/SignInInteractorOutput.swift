import Foundation

protocol SignInInteractorOutput: class {
    
    func changeLoadingState()
    func showErrorAuthorizationAlert()
    func closeView()
}
