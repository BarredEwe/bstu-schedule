import UIKit
import RealmSwift

protocol MGObjectMapperProtocol {
    
    static func objectsFromResponce(responseObject: AnyObject, type: String) -> [MGObject]?
    static func objectFromRespocne(responseObject: [String: AnyObject]) -> MGObject
    static func convertTo<T: Object, C: Object>(objects: Results<T>, toType: C) -> [C]
    static func isEqual(array : [MGObject], array1 : [MGObject]) -> Bool
}

class MGObjectMapper: MGObjectMapperProtocol {
    
    class func objectFromRespocne(responseObject: [String: AnyObject]) -> MGObject {
        let object = MGObject()
        object.name = responseObject["name"] as! String
        if let id = responseObject["id"] as? String {
            object.id = id
        }
        return object
    }
    
    class func convertTo<T: Object, C: Object>(objects: Results<T>, toType: C) -> [C] {
        var toObjects = [C]()
        for object in objects {
            toObjects.append(object as! C)
        }
        return toObjects
    }
    
    class func objectsFromResponce(responseObject: AnyObject, type: String) -> [MGObject]? {
        
        var objects = [MGObject]()
        
        guard let items: [Dictionary<String, AnyObject>] = responseObject.object(forKey: type) as? [Dictionary<String, AnyObject>] else {
            return nil
        }
        
        for item in items {
            let object = MGObjectMapper.objectFromRespocne(responseObject: item)
            objects.append(object)
        }
        
        return objects
    }
    
    class func isEqual(array : [MGObject], array1 : [MGObject]) -> Bool {
        
        if array.count != array1.count {
            return false
        }
        
        let arraySorted = array.sorted( by: {$0.name > $1.name} )
        let array1Sorted = array1.sorted( by: {$0.name > $1.name} )
        
        for obj in arraySorted {
            let index = arraySorted.index(of: obj)
            let obj1 = array1Sorted[index!]
            
            if obj.id != obj1.id {
                return false
            }
        }
        
        return true
    }

    
}
