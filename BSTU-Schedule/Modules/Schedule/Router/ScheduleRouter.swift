import UIKit

class ScheduleRouter: ScheduleRouterInput {
    
    weak var view: UIViewController?

    func navigateToExtendedScheduleWith(event: Event, filter: Filter) {
        let extendedScheduleView = ExtendedScheduleAssembly.assemblyWith(event: event, filter: filter)
        self.view?.navigationController?.pushViewController(extendedScheduleView, animated: true)
    }
    
}
