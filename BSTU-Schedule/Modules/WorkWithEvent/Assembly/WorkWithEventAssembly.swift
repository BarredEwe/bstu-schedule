import UIKit

class WorkWithEventAssembly {
    
    class func assembly() -> UIViewController {
        let storyBoard = UIStoryboard(name: "WorkWithEventView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! WorkWithEventViewController
        
        let router = WorkWithEventRouter()
        router.view = vc
        
        let presenter = WorkWithEventPresenter()
        presenter.view = vc
        presenter.router = router
        
        let interactor = WorkWithEventInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
