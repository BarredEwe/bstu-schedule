
class ExtendedSchedulePresenter: ExtendedScheduleModuleInput, ExtendedScheduleViewOutput, ExtendedScheduleInteractorOutput {

    weak var view: ExtendedScheduleViewInput!
    var interactor: ExtendedScheduleInteractorInput!
    var router: ExtendedScheduleRouterInput!

    func viewIsReady() {

    }
    
    func getEvent() -> Event {
        return self.interactor.getEvent()
    }
    
    func getFilter() -> Filter {
        return self.interactor.getFilter()
    }
    
    func navigationBack() {
        self.router.navigationBack()
    }
}
