import UIKit

class CustomScheduleHeader: UICollectionReusableView {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(day: Day, week: Int) {
        let date = day.date
        
        let day = date?.startOfWeek!
        let newdate = Calendar.current.date(byAdding: .day, value: week, to: day!)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        
        let weekString = formatter.string(from: newdate)
        
        
        formatter.dateFormat = "d MMMM"
        let dateString = formatter.string(from: newdate)
        
        info.text = weekString
        dateLabel.text = dateString
    }

}
