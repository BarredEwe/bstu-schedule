import RealmSwift

class LessonTypeDB: MGObjectDB {
    
    func fetchLessonTypes<T: MGObject>(objects: [T]) {
        let newDisciples = LessonTypeMapper.objectsFrom(objects: objects)
        super.fetchObjects(groups: newDisciples)
    }
    
    func getLessonType(filter: String) -> [LessonType]? {
        return super.getObjects(filter: filter, type: LessonType())
    }
    
}
