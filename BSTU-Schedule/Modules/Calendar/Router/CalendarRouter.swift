import UIKit

class CalendarRouter: CalendarRouterInput {
    
    weak var view: UIViewController?
    
    func navigateToShowDayScheduleWith(filter: Filter) {
        let scheduleView = ScheduleContainerAssembly.assemblyWith(filter: filter)
        self.view?.navigationController?.pushViewController(scheduleView, animated: true)

    }
    
    func navigateBack() {
        view?.dismiss(animated: true, completion: nil)
    }
    
}
