import RealmSwift

class RoomDB: MGObjectDB {
    
    func fetchRooms<T: Object>(groups: [T]) {
        super.fetchObjects(groups: groups)
    }
    
    func getRooms(filter: String) -> [Room]? {
        return super.getObjects(filter: filter, type: Room())
    }
    
}
