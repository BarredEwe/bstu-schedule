import UIKit

class SignInInteractor: SignInInteractorInput {

    weak var output: SignInInteractorOutput!

    func userAuthorization(userName: String, password: String) {
        APIUser.authorization(userName: userName, password: password, success: {
            self.output.changeLoadingState()
            self.output.closeView()
        }, failed: { error in
            self.output.changeLoadingState()
            self.output.showErrorAuthorizationAlert()
        })
    }
}
