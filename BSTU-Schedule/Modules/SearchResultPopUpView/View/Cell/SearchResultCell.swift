//
//  SearchResultCell.swift
//  BSTU-Schedule
//
//  Created by Максим Гришутин on 30.05.17.
//  Copyright © 2017 Максим Гришутин. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
