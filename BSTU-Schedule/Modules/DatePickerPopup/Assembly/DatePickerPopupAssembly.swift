import UIKit

class DatePickerPopupAssembly {
    
    class func assemblyWith(searchDelegate: SearchResultDelegate) -> DatePickerPopupViewController {
        
        let storyBoard = UIStoryboard(name: "DatePickerPopupView", bundle: nil)
        let view = storyBoard.instantiateInitialViewController() as! DatePickerPopupViewController
        
        let router = DatePickerPopupRouter()
        router.view = view
        
        let presenter = DatePickerPopupPresenter()
        presenter.router = router
        presenter.view = view
        
        view.output = presenter
        
        let interactor = DatePickerPopupInteractor()
        interactor.output = presenter
        interactor.setDelegate(delegate: searchDelegate)
        
        presenter.interactor = interactor
        
        return view
    }
    
}
