protocol WorkWithEventViewOutput {

    func viewIsReady()
    func searchWith(type: TypeSearch)
    func showSucсefullMessage()
    
}
