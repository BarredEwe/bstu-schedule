import UIKit

class ExtendedScheduleInteractor: ExtendedScheduleInteractorInput {

    weak var output: ExtendedScheduleInteractorOutput!
    var event = Event()
    var filter = Filter()
    
    func setEvent(event: Event) {
        self.event = event
    }
    
    func getEvent() -> Event {
        return event
    }
    
    func setFilter(filter: Filter) {
        self.filter = filter
    }
    
    func getFilter() -> Filter {
        return filter
    }

}
