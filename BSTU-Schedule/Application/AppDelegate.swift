import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        registerNotifications()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.black
        
        let storyBoard = UIStoryboard(name: "SplashScreen", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! SplashScreenViewController
        
        let navigationController = UINavigationController(rootViewController: vc)
        self.window!.rootViewController = navigationController
        self.window!.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        
        DayDB().removeOldObjects()
        scheduleNotification()
        
        return true
    }
    
    func registerNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            if granted {
                UIApplication.shared.registerForRemoteNotifications()
            } else {
                print("Something went wrong")
            }
        }
    }
    
    @available(iOS 10, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
    func scheduleNotification() {
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        let timeDateFormatter = DateFormatter()
        timeDateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        guard let user = UserDB().getUser() else {
            return
        }
        
        let filter = UserMapper.getFilterFrom(user: user)
        guard let days = DayDB().getObjects(filter: filter) else {
            return
        }
        
        var i = 0
        for day in days {
            for event in day.events {
                
                let day = event.startDate?.startOfWeek!
                let newdate = Calendar.current.date(byAdding: .day, value: event.day, to: day!)!
                
                let content = UNMutableNotificationContent()
                content.title = (event.lesson?.name)!
                
                content.subtitle = (event.groups.first?.name)!
                content.body = (event.teachers.first?.name)!
                
                content.sound = UNNotificationSound.default()
                
                let str1 = dateFormatter.string(from: newdate)
                let str2 = timeFormatter.string(from: event.startTime!)
                let id = event.id
                
                let date = timeDateFormatter.date(from: "\(str1) \(str2)")
                
                var triggerDate = Calendar.current.dateComponents([/*.year,*/ .weekday,.hour,.minute], from: date!)
                triggerDate.timeZone = NSTimeZone.default
                triggerDate.second = 0
                
                let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: true)
                let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error : Error?) in
                    if let theError = error {
                        print(theError.localizedDescription)
                    }
                }
            }
            i += 1
        }
    }
    
}

