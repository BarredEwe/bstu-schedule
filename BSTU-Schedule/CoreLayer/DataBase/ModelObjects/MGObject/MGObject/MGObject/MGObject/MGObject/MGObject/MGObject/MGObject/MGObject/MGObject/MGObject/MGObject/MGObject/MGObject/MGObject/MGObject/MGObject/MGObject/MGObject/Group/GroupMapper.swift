import UIKit

protocol GroupMapperProtocol {
    static func objectFrom(object: MGObject) -> Group
}

class GroupMapper: MGObjectMapper, GroupMapperProtocol {
    
    class func objectFrom(object: MGObject) -> Group {
        let group = Group()
        group.name = object.name
        group.id = object.id
        return group
    }
    
}
