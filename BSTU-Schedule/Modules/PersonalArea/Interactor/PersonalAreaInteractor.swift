//
//  PersonalAreaPersonalAreaInteractor.swift
//  BSTU-Schedule
//
//  Created by Maxim on 12/04/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

class PersonalAreaInteractor: PersonalAreaInteractorInput {

    weak var output: PersonalAreaInteractorOutput!
    
    func logoutUser() {
        APIUser.logoutUser()
    }
    
    func clearChache() {
        DayDB().clearChacheDays()
    }

}
