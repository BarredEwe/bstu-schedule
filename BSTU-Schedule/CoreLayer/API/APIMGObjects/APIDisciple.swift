import UIKit

class APIDisciple: APIMGObject {
    
    class func getDiscipleWith(name: String, success: @escaping ([MGObject]?) -> ()) {
        
        let objectsFromDB = DiscipleDB().getDesciples(filter: name)
        success(objectsFromDB)
        
        self.getMGObjectWith(type: "lecture", name: name, success: { responseObject in
            
            let objects = MGObjectMapper.objectsFromResponce(responseObject: responseObject, type: "data")
            
            if objects != nil {
                DispatchQueue.global(qos: .userInitiated).async {
                    DiscipleDB().fetchDesicples(objects: objects!)
                }
                if !MGObjectMapper.isEqual(array: objectsFromDB!, array1: objects!) {
                    success(objects)
                }
            }
        }, failure: { error in
            
        })
    }
    
}
