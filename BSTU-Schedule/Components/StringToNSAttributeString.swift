import UIKit

class StringToAttributeString {
    
    class func stringChangeAttr(string: String, searchString: String, font: UIFont = UIFont.boldSystemFont(ofSize: 14)) -> NSAttributedString {
        
        let mtString = NSMutableAttributedString(string: string)
        
        let lowString = string.lowercased() as NSString
        
        let searchString = searchString.lowercased()
        
        mtString.setAttributes([NSAttributedStringKey.font: font,
                                NSAttributedStringKey.foregroundColor: UIColor.white],
                               range: lowString.range(of: searchString))
        return mtString
    }
    
}
