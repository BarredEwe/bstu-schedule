import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var name = ""
    @objc dynamic var type = ""
    @objc dynamic var token = ""
    @objc dynamic var data = ""
}
