import UIKit

protocol FilterViewInput: class {

    func setupInitialState()
    func updateDate()
    func updateName()
}
