import Foundation

protocol ScheduleInteractorOutput: class {
    
    func updateSchedule()

}
