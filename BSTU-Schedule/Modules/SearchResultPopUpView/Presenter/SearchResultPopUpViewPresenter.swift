import UIKit

class SearchResultPopUpViewPresenter: SearchResultPopUpViewModuleInput, SearchResultPopUpViewViewOutput, SearchInteractorOutput {

    weak var view: SearchResultPopUpViewViewInput!
    var interactor: SearchInteractor!
    var router: SearchResultPopUpViewRouterInput!

    func viewIsReady() {

    }
    
    func showResult() {
        self.view.updateResults()
    }
    
    func startSearchWith(data: String, type: TypeSearch) {
        self.interactor.setType(type: type)
        self.interactor.searchWith(info: data)
    }
    
    func getObjects() -> [MGObject] {
        return self.interactor.getObjects()
    }
    
    func didFinishSearch(index: Int) {
        self.interactor.sendResult(info: self.getObjects()[index].name)
    }
    
    func getLastSearch() -> String {
        return self.interactor.getLastSearch()
    }
}
