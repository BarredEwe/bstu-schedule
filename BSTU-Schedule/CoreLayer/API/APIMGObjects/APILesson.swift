import UIKit

class APILesson: APIMGObject {
    
    class func getLessonsWith(name: String, success: @escaping ([MGObject]?) -> ()) {
        
        let objectsFromDB = LessonDB().getLessons(filter: name)
        success(objectsFromDB)
        
        self.getMGObjectWith(type: "subject", name: name, success: { responseObject in
            
            guard let objects = MGObjectMapper.objectsFromResponce(responseObject: responseObject, type: "data") else {
                return
            }
            
            if !MGObjectMapper.isEqual(array: objectsFromDB!, array1: objects) {
                DispatchQueue.global(qos: .userInitiated).async {
                    LessonDB().fetchLessons(lessons: objects)
                }
                success(objects)
            }
            
        }, failure: { error in
            
        })
    }
    
}
