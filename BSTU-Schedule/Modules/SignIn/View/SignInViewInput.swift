protocol SignInViewInput: class {

    func setupInitialState()
    func changeLoadingState()
}
