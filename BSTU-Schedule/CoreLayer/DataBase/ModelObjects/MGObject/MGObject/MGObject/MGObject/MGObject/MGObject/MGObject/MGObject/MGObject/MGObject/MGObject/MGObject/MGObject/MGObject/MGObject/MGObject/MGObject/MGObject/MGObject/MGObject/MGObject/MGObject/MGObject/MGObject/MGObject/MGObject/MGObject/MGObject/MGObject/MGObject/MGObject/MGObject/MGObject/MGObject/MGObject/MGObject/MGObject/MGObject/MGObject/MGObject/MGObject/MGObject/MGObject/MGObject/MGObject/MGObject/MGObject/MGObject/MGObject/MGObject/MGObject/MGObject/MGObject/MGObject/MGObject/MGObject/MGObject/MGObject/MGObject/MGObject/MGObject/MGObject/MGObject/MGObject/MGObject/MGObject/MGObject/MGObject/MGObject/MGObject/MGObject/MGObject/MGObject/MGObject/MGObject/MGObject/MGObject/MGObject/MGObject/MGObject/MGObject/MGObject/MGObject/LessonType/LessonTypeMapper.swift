import UIKit

protocol LessonTypeMapperProtocol {
    static func objectFrom(object: MGObject) -> LessonType
}

class LessonTypeMapper: MGObjectMapper, LessonTypeMapperProtocol {
    
    class func objectFrom(object: MGObject) -> LessonType {
        let disciple = LessonType()
        disciple.name = object.name
        disciple.id = object.id
        return disciple
    }
    
    class func objectsFrom(objects: [MGObject]) -> [LessonType] {
        var disciples = [LessonType]()
        for object in objects {
            let disciple = LessonType()
            disciple.name = object.name
            disciple.id = object.id
            disciples.append(disciple)
        }
        
        return disciples
    }
    
}
