import UIKit

class DatePickerPopupViewController: UIViewController, DatePickerPopupViewInput, UIPickerViewDelegate, UIPickerViewDataSource {

    var output: DatePickerPopupViewOutput!

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!

    let formatter = DateFormatter()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        output.loadAllClassTime()
        
        formatter.dateFormat = "HH:mm"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOpacity = 1
        backView.layer.shadowOffset = CGSize.zero
        backView.layer.shadowRadius = 10
    }

    // MARK: DatePickerPopupViewInput
    func setupInitialState() {
        
    }
    
    func reloadPickerView() {
        self.pickerView.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.output.getAllCassTime().count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return formatter.string(from: self.output.getAllCassTime()[row].start!)
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        let index = pickerView.selectedRow(inComponent: 0)
        self.output.sendResultWith(index: index)
        self.output.navigateBack()
    }
}
