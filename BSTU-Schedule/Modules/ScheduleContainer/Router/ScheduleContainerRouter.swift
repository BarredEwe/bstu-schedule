class ScheduleContainerRouter: ScheduleContainerRouterInput {
    
    weak var view: ScheduleContainerViewController?
    
    func navigateToCalendarWith(filter: Filter, delegate: SearchResultDelegate) {
        
        let vc = CalendarAssembly.assemblyWith(filter: filter, delegate: delegate)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        view?.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
}
