import Foundation

protocol WorkWithEventRouterInput {
    
    func searchWith(type: TypeSearch, delegate: SearchResultDelegate)
    func showSucсefullMessage(actionOk: @escaping () -> ())
    func navigationBack() 
}
