import Foundation

protocol FilterInteractorInput {
    
    func setFilter(type: TypeUser)
    func setFilter(data: String)
    func setFilter(day: Date)
    func getFilter() -> Filter

}
