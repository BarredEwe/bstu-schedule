import UIKit

/// Works with user authorization.
class APIUser: APIClient {
    
    /**
     Creates an event on the server.
     
     - returns: Returns true if the user is authorized, otherwise returns false.
     */
    class func chechAuthorize() -> Bool {
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey: "userLoggedIn") == nil {
            return false
        } else {
            return true
        }
    }
    
    /**
     Sets the authorization flag to enabled.
     */
    class func updateUserLoggedInFlag() {
        let defaults = UserDefaults.standard
        defaults.set("loggedIn", forKey: "userLoggedIn")
        defaults.synchronize()
    }
    
    /**
     Sets the authorization flag to disabled.
     */
    class func updateUserLogOutInFlag() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "userLoggedIn")
        defaults.synchronize()
    }
    /**
     User authorization.
     
     - Parameter userName: The name of the user who will be authorized.
     - Parameter password: User password.
     - Parameter success: Successful user authorization.
     - Parameter failure: A failed user authorization.
     */
    class func authorization(userName: String, password: String, success: @escaping () -> (), failed: @escaping (NSError?) -> ()) {
        let authURL = "auth"
        
        let parametres = [
            "name": userName,
            "pass": password
        ]
        
        self.requset(methodType: .post, URL: authURL, headers: nil, parametres: parametres, success: { respondeObject in
            let user = UserMapper.objectFromRespocne(responseObject: respondeObject)
            if user == nil {
                failed(nil)
                return
            }
            UserDB().fetch(user: user!)
            updateUserLoggedInFlag()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserAction"), object: nil)
            success()
        }) { error in
            failed(error)
            print(error)
        }
        
    }
    
    /**
     Deauthorization of the user and send notification about this.
     */
    class func logoutUser() {
        updateUserLogOutInFlag()
        UserDB().removeObject(object: UserDB().getUser()!)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserAction"), object: nil)
    }
    
}
