//
//  ExtendedScheduleExtendedScheduleRouterInput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 03/05/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

import Foundation

protocol ExtendedScheduleRouterInput {

    func navigationBack()
    
}
