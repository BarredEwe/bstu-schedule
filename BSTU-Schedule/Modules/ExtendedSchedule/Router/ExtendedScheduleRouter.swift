import UIKit

class ExtendedScheduleRouter: ExtendedScheduleRouterInput {
    
    weak var view: UIViewController?
    
    func navigationBack() {
        view?.navigationController?.popViewController(animated: true)
    }

}
