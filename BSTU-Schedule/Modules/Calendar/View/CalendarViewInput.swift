//
//  CalendarCalendarViewInput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 06/04/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

protocol CalendarViewInput: class {
    
    func setupInitialState()
    func updateCalendar()
    
}
