import UIKit
protocol ScheduleViewOutput {
    
    func viewIsReady()
    func days() -> [Day]
    func loadDay()
    func didSelectEventAt(index: Int, section: Int)
    
}
