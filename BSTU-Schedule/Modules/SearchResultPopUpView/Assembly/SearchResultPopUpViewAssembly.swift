import UIKit

protocol SearchResultDelegate {
    func didFinisedSearchWith(result: Any)
}

class SearchResultPopUpViewAssembly {
    
    class func assemblyWith(searchDelegate: SearchResultDelegate) -> SearchResultPopUpView {
        
        let view = UINib(nibName: "SearchResultPopUpView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! SearchResultPopUpView
        
        let router = SearchResultPopUpViewRouter()
        
        let presenter = SearchResultPopUpViewPresenter()
        presenter.router = router
        presenter.view = view
        
        view.output = presenter
        
        let interactor = SearchInteractor()
        interactor.output = presenter
        interactor.setDelegate(delegate: searchDelegate)
        
        presenter.interactor = interactor
        
        return view
    }
    
}
