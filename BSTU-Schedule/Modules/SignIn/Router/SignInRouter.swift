import UIKit

class SignInRouter: SignInRouterInput {
    
    weak var view: UIViewController?
    
    func closeView() {
        view?.dismiss(animated: true, completion: nil)
    }
    
    func showErrorAuthorizationAlert() {
        let alertView = UIAlertController(title: "Ошибка авторизации",
                                          message: "Неверный логин или пароль" as String, preferredStyle:.alert)
        let okAction = UIAlertAction(title: "Ввести заново", style: .default, handler: nil)
        alertView.addAction(okAction)
        view?.present(alertView, animated: true, completion: nil)
    }

}
