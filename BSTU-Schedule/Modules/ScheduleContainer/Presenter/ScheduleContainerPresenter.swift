import UIKit

class ScheduleContainerPresenter: ScheduleContainerModuleInput, ScheduleContainerViewOutput, ScheduleContainerInteractorOutput, SearchResultDelegate {

    weak var view: ScheduleContainerViewInput!
    var interactor: ScheduleContainerInteractorInput!
    var router: ScheduleContainerRouterInput!

    func viewIsReady() {
        createPages()
    }
    
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(userActions), name: NSNotification.Name(rawValue: "UserAction"), object: nil)
    }
    
    func getPageAfter(viewController: UIViewController) -> UIViewController? {
        return self.interactor.getPageAfter(viewController: viewController)
    }
    
    func getPageBefore(viewController: UIViewController) -> UIViewController? {
        return self.interactor.getPageBefore(viewController: viewController)
    }
    
    func getPages() -> [UIViewController] {
        return self.interactor.getPages()
    }

    func createPages() {
        self.interactor.createPages()
    }
    
    func showCalendar() {
        self.router.navigateToCalendarWith(filter: self.interactor.getFilter(), delegate: self)
    }
    
    func didFinisedSearchWith(result: Any) {
        self.interactor.setFilter(day: result as! Date)
        self.view.refreshPageViewController()
    }
    
    func didFinisedSearchWith(result: String) { }
    
    @objc func userActions() {
        if !APIUser.chechAuthorize() {
            return
        }
        
        let user = UserDB().getUser()!
        let filter = UserMapper.getFilterFrom(user: user)
        
        if user.type == "Студент" {
            filter.type = .teacher
            filter.data = user.data
        } else {
            filter.type = .disciple
            filter.data = user.name
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        filter.day = Date()
        
        self.interactor.setFilter(filter: filter)
        self.view.refreshPageViewController()
    }
}
