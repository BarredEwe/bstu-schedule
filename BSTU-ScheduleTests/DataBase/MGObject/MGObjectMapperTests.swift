import XCTest
@testable import BSTU_Schedule

class MGObjectMapperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testObjectFromRespocne() {
        let responseObject = ["name": "А" as AnyObject,
                              "id": "1" as AnyObject]
        
        let object = MGObjectMapper.objectFromRespocne(responseObject: responseObject)
        XCTAssertNotNil(object, "Object from mapper is nil")
        XCTAssertNotNil(object.id, "Object id from mapper is nil")
        XCTAssertNotNil(object.name, "Object name from mapper is nil")
    }
    
    func testObjectsFromResponce() {
        let responseObjects = ["message": "OK", "lecturers": [["name": "А", "id": "1"],["name": "B", "id": "2"]]] as [String : Any]
        
        let object = MGObjectMapper.objectsFromResponce(responseObject: responseObjects as AnyObject, type: "lecturers")
        XCTAssertNotNil(object?.first, "Object from mapper is nil")
        XCTAssertNotNil(object?.first?.id, "Object id from mapper is nil")
        XCTAssertNotNil(object?.first?.name, "Object name from mapper is nil")
    }
    
    func testEqualObjects() {
        let object = MGObject()
        object.id = "0"
        object.name = "0"
        let object1 = MGObject()
        object1.id = "1"
        object1.name = "1"
        
        var firstArray = [object, object1]
        var secondArray = [object1, object]
        
        var isEqual = MGObjectMapper.isEqual(array: firstArray, array1: secondArray)
        XCTAssertTrue(isEqual, "Elements must be equal")
        
        firstArray = [object1]
        secondArray = [object1, object, object1]
        
        isEqual = MGObjectMapper.isEqual(array: firstArray, array1: secondArray)
        XCTAssertFalse(isEqual, "Elements must be not equal")
    }
    
}
