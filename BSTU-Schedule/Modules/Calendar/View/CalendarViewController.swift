import UIKit
import JTAppleCalendar

class CalendarViewController: UIViewController, CalendarViewInput, JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {

    var output: CalendarViewOutput!
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var year: UILabel!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCalendarView()
        output.viewIsReady()
    }
    
    // MARK: Setup
    func setupCalendarView() {
        calendarView.allowsMultipleSelection = true
        calendarView.scrollingMode = .stopAtEachSection
        
        calendarView.layer.cornerRadius = 8
        calendarView.layer.shadowColor = UIColor.black.cgColor
        calendarView.layer.shadowOpacity = 1
        calendarView.layer.shadowOffset = CGSize.zero
        calendarView.layer.shadowRadius = 10
        
        self.updateCalendar()
    }

    // MARK: CalendarViewInput
    func setupInitialState() {
        
    }
    
    // MARK: Calendar update
    func updateCalendar() {
        self.configureWith(date: Date())
        self.calendarView.scrollToDate(Date(), triggerScrollToDateDelegate: false, animateScroll: false, preferredScrollPosition: nil, extraAddedOffset: 0, completionHandler: nil)
        //self.calendarView.selectDates(self.output.selectedDates(), triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: true)
    }
    
    // MARK: Calendar DataSource
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let startDate = self.output.startDate()
        let endDate = self.output.endDate()
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfRow, firstDayOfWeek: .monday, hasStrictBoundaries: true)
        return parameters
    }
    
    // MARK: Calendar Delegate
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CellView
        cell.dayLabel.text = cellState.text
        
        if Calendar.current.isDateInToday(date) {
             cell.currentDay.isHidden = false
        } else {
             cell.currentDay.isHidden = true
        }
        
        if cellState.dateBelongsTo == .thisMonth {
             cell.isUserInteractionEnabled = true
             cell.dayLabel.textColor = UIColor.black
        } else {
             cell.isUserInteractionEnabled = false
             cell.dayLabel.textColor = UIColor.lightGray
        }
        
        if cellState.isSelected {
            cell.selectedView.isHidden = false
        } else {
            cell.selectedView.isHidden = true
        }
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.output.viewDidSelect(date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.output.viewDidSelect(date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first?.date
        configureWith(date: date!)
    }
    
    func configureWith(date: Date) {
        let month = Calendar.current.dateComponents([.month], from: date).month!
        
        let monthName = DateFormatter().standaloneMonthSymbols[(month - 1) % 12]
        let year = Calendar.current.component(.year, from: date)
        
        self.month.text = monthName.uppercased()
        self.year.text = String(year)
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.output.navigateBack()
    }
    
    
}
