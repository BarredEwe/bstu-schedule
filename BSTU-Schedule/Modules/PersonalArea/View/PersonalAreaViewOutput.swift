//
//  PersonalAreaPersonalAreaViewOutput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 12/04/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

protocol PersonalAreaViewOutput {
    
    func viewIsReady()
    func navigateToCreateEvent()
    func userLogout()
    func navigateToLogin()
    func clearChache()
    
}
