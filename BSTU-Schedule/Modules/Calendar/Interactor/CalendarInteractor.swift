import UIKit

class CalendarInteractor: CalendarInteractorInput {
    
    weak var output: CalendarInteractorOutput!
    
    var dates = [Date]()
    var filter: Filter?
    var delegate: SearchResultDelegate?
    
    func setFilter(filter: Filter) {
        self.filter = filter
    }
    
    func startDate() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"

        return formatter.date(from: "2015 01 01")!
    }
    
    func endDate() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        return formatter.date(from: "2025 12 01")!
    }
    
    func getFilterWith(date: Date) -> Filter {
        filter?.day = date
        return filter!
    }
    
    func selectedDates() -> [Date] {
        if dates.count == 0 {
            for i in 0...8 {
                dates.append(Calendar.current.date(byAdding: .day, value: i, to: Date())!)
            }
        }
        return dates
    }
    
    func setDelegate(delegate: SearchResultDelegate?) {
        if delegate != nil {
            self.delegate = delegate
        }
    }
    
    func sendResult(date: Date) -> Bool {
        if delegate != nil {
            delegate?.didFinisedSearchWith(result: date)
            return true
        }
        return false
    }
}
