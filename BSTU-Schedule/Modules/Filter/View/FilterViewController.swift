import UIKit

class FilterViewController: UIViewController, FilterViewInput {
    
    var output: FilterViewOutput!
    
    @IBOutlet weak var name: UIButton!
    @IBOutlet weak var group: UIButton!
    @IBOutlet weak var date: UIButton!
    @IBOutlet var type: [UIButton]!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    let formatter = DateFormatter()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    // MARK: FilterViewInput
    func setupInitialState() {
        
    }
    
    func updateType() {
        var constraint: CGFloat = 0
        var alpha1: CGFloat = 1.0
        var alpha2: CGFloat = 0.4
        
        if self.output.getFilter().type == .teacher {
            constraint = -self.view.bounds.width
            alpha1 = 0.4
            alpha2 = 1
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.type[0].alpha = alpha1
            self.type[1].alpha = alpha2
        })
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
            self.leftConstraint.constant = constraint
            self.view.layoutIfNeeded()
        }, completion: nil)
        checkSearch()
    }
    
    func updateName() {
        let filter = self.output.getFilter()
        
        if filter.data != "" {
            if filter.type == .disciple {
                name.setTitle(filter.data, for: .normal)
                name.isSelected = true
            } else {
                group.setTitle(filter.data, for: .normal)
                group.isSelected = true
            }
        }
        checkSearch()
    }
    
    func updateDate() {
        let filter = self.output.getFilter()
        
        guard let day = filter.day else {
            return
        }
        
        formatter.dateFormat = "EEE d MMM YYYY"
        let dateStr = formatter.string(from: day)
        date.setTitle("\(dateStr)", for: .normal)
        date.isSelected = true
        
        checkSearch()
    }
    
    func checkSearch() {
        var check = false
        let filter = self.output.getFilter()
        
        if filter.type == .disciple {
            if self.name.state == .selected {
                check = true
            }
        } else {
            if self.group.state == .selected {
                check = true
            }
        }
        
        if check && filter.day != nil{
            UIView.animate(withDuration: 0.2, animations: {
                self.searchButton.alpha = 1
                self.searchButton.isEnabled = true
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.searchButton.alpha = 0.4
                self.searchButton.isEnabled = false
            })
        }
    }
    
    @IBAction func touchTypeButton(_ sender: UIButton) {
        if sender.tag == 1 {
            self.output.setFilter(type: .disciple)
            if self.name.state == .selected {
                self.output.setFilter(data: self.name.currentTitle!)
            }
        } else {
            self.output.setFilter(type: .teacher)
            if self.group.state == .selected {
                self.output.setFilter(data: self.group.currentTitle!)
            }
        }
        updateType()
    }
    
    @IBAction func touchNameButton(_ sender: UIButton) {
        if self.output.getFilter().type == .disciple {
            self.output.searchWith(type: .disciple)
        } else {
            self.output.searchWith(type: .group)
        }
    }
    
    @IBAction func touchDateButton(_ sender: UIButton) {
        self.output.searchDate()
    }
    
    @IBAction func touchNextButton(_ sender: UIButton) {
        if self.output.getFilter().type == .disciple {
            self.output.setFilter(data: self.name.currentTitle!)
        } else {
            self.output.setFilter(data: self.group.currentTitle!)
        }
        self.output.nextButtonDidPressed()
    }
}
