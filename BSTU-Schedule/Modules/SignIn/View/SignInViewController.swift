import UIKit

class SignInViewController: UIViewController, SignInViewInput {
    
    var output: SignInViewOutput!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var waitIndicator: UIActivityIndicatorView!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupKeyboard()
        output.viewIsReady()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: SignInViewInput
    func setupInitialState() {
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: Keyborad Setup
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // MARK: Keyborad notofication
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if mainViewCenterConstraint.constant == 0{
                mainViewCenterConstraint.constant -= keyboardSize.height / 3
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if mainViewCenterConstraint.constant != 0{
                mainViewCenterConstraint.constant += keyboardSize.height / 3
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    // MARK: Actions
    @IBAction func loginAction(_ sender: UIButton) {
        if (userNameTextField.text != "" || passwordTextField.text != "") {
            changeLoadingState()
            self.output.userAuthorization(userName: userNameTextField.text!, password: passwordTextField.text!)
            dismissKeyboard()
        } else {
            self.output.showErrorAuthorizationAlert()
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.output.closeView()
    }
    
    // MARK: UpdateWaitIndicator
    func changeLoadingState() {
        if waitIndicator.isAnimating {
            waitIndicator.stopAnimating()
            UIView.animate(withDuration: 0.3, animations: {
                self.mainView.alpha = 1
            })
        } else {
            waitIndicator.startAnimating()
            UIView.animate(withDuration: 0.3, animations: {
                self.mainView.alpha = 0
            })
        }
    }
    
}
