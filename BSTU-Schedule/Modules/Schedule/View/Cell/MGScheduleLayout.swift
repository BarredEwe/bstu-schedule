import UIKit

protocol MGScheudleLayoutDelegate {
    
}

class MGScheudleLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        
        self.scrollDirection = .horizontal
        self.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 65)
        self.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 80)
        self.sectionInset = UIEdgeInsetsMake(65, 0, 0, 0)
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
/*    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var answer = super.layoutAttributesForElements(in: rect)
        
        let cv = self.collectionView!
        let contentOffset = cv.contentOffset
        
        let missingSections = NSMutableIndexSet()
        for layoutAttributes in answer! {
            if layoutAttributes.representedElementCategory == UICollectionElementCategory.cell {
                missingSections.add(layoutAttributes.indexPath.section)
            }
        }
        for layoutAttributes in answer! {
            if layoutAttributes.representedElementKind == UICollectionElementKindSectionHeader {
                missingSections.remove(layoutAttributes.indexPath.section)
            }
        }
        
        missingSections.enumerate(using: { (idx, stop) in
            let indexPath = IndexPath(item: 0, section: idx)
            let layoutAttributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath)
            answer?.append(layoutAttributes!)
        })
        
        for layoutAttributes in answer! {
            if layoutAttributes.representedElementKind == UICollectionElementKindSectionHeader {
                let section = layoutAttributes.indexPath.section
                let numberOfItemsInSection = cv.numberOfItems(inSection: section)
                
                let firstCellIndexPath = IndexPath(item: 0, section: section)
                let lastCellIndexPath = IndexPath(item: numberOfItemsInSection - 1, section: section)
                
                let firstCellAttrs = self.layoutAttributesForItem(at: firstCellIndexPath)
                let lastCellAttrs = self.layoutAttributesForItem(at: lastCellIndexPath)
                
                if self.scrollDirection == .vertical {
                    let headerHeight = layoutAttributes.frame.height
                    var origin = layoutAttributes.frame.origin
                    origin.y = max(contentOffset.y, (firstCellAttrs?.frame.minY)! - headerHeight, (lastCellAttrs?.frame.maxY)! - headerHeight)
                    layoutAttributes.zIndex = -1
                    layoutAttributes.frame = CGRect(origin: origin, size: layoutAttributes.frame.size)
                } else {
                    let headerWidth = layoutAttributes.frame.width
                    var origin = layoutAttributes.frame.origin
                    origin.x = max(contentOffset.x, (firstCellAttrs?.frame.minX)! - headerWidth, (lastCellAttrs?.frame.maxX)! - headerWidth)
                    layoutAttributes.zIndex = -1
                    layoutAttributes.frame = CGRect(origin: origin, size: layoutAttributes.frame.size)
                }
            }
        }
        return answer
    }
    */
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
}


