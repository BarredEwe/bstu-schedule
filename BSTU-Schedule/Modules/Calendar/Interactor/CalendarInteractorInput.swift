import Foundation

protocol CalendarInteractorInput {
    
    func setFilter(filter: Filter)
    func startDate() -> Date
    func endDate() -> Date
    func getFilterWith(date: Date) -> Filter
    
    func selectedDates() -> [Date]
    func setDelegate(delegate: SearchResultDelegate?)
    func sendResult(date: Date) -> Bool
    
}
