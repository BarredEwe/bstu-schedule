protocol SearchResultPopUpViewViewInput: class {
    
    func setupInitialState()
    
    func updateResults()

}
