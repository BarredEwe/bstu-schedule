import UIKit

class ScheduleInteractor: ScheduleInteractorInput {

    weak var output: ScheduleInteractorOutput!
    var days = [Day]()
    
    var filter: Filter?
    
    func setFilter(filter: Filter) {
        self.filter = filter
    }
    
    func getDays() -> [Day] {
        return days
    }
    
    func getFilter() -> Filter {
        return self.filter!
    }
    
    func loadDay() { //TODO: Вывод из базы данных на экран, без убирания кружка загрузки
         APIDays.getDaysWith(filter: filter!, success: { [unowned self] days, typeResponce in
            if days != nil {
                self.days = days!
                self.output.updateSchedule()
            } else {
                if typeResponce == .Server {
                    self.days.removeAll()
                    self.output.updateSchedule()
                }
            }
        }) { error in
            self.output.updateSchedule()
        }
    }

}
