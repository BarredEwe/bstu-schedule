import UIKit

enum TypeSearch {
    case lesson   //Предмет
    case group  //Группа
    case room   //Аудитория
    case disciple   //Преподователь
    case typeLesson   //Типы занятий
}

class SearchInteractor: SearchInteractorInput {

    weak var output: SearchInteractorOutput!
    var type: TypeSearch = .disciple
    var delegate: SearchResultDelegate?
    var lastSearch = ""
    
    var objects = [MGObject]()
    
    func setType(type: TypeSearch) {
        self.type = type
    }
    
    func getTitle() -> String {
        var text = ""
        switch type {
        case .disciple:
            text = "Поиск преподователя"
        case .group:
            text = "Поиск группы"
        case .lesson:
            text = "Поиск предмета"
        case .room:
            text = "Поиск аудитории"
        case .typeLesson:
            text = ""
        }
        return text
    }
    
    func setDelegate(delegate: SearchResultDelegate) {
        self.delegate = delegate
    }
    
    func sendResult(info: Any) {
        delegate?.didFinisedSearchWith(result: info)
    }
    
    func getObjects() -> [MGObject] {
        return objects
    }
    
    func getLastSearch() -> String {
        return lastSearch
    }
    
    func searchWith(info: String) {
        lastSearch = info
        if info == "" {
            self.searchIsLoaded(objects: nil)
            return
        }
        switch type {
        case .disciple:
            APIDisciple.getDiscipleWith(name: info, success: { objects in
                self.searchIsLoaded(objects: objects)
            })
            break
        case .group:
            APIGroup.getGroupsWith(name: info, success: { objects in
                self.searchIsLoaded(objects: objects)
            })
            break
        case .lesson:
            APILesson.getLessonsWith(name: info, success: { objects in
                self.searchIsLoaded(objects: objects)
            })
            break
        case .room:
            APIRoom.getRoomsWith(name: info, success: { objects in
                self.searchIsLoaded(objects: objects)
            })
            break
        case .typeLesson:
            APILessonType.getLessonTypeWith(name: info, success: { objects in
                self.searchIsLoaded(objects: objects)
            })
            break
        }
    }
    
    func searchIsLoaded(objects: [MGObject]?) {
        if objects == nil {
            self.objects.removeAll()
        } else {
            self.objects = objects!
        }
        self.output.showResult()
    }
}
