import UIKit

extension Date {
    
    func evenOddWeek() -> Int {
        // 1 - нечетная, 2 - четная
        let weekNum = Calendar.current.component(.weekOfYear, from: self)
        let week = 32 - weekNum
        
        var drop = 1
        if week % 2 == 0 { // четная
            drop = 2
        }
        return drop
    }
    
    
    struct Gregorian {
        static let calendar = Calendar(identifier: .gregorian)
    }
    
    var startOfWeek: Date? {
        let date =  Gregorian.calendar.date(from: Gregorian.calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        
        return Calendar.current.date(byAdding: .day, value: 1, to: date!)
    }
    
    func checkIsContainOfWeek(date: Date, dateStart: Date, dateEnd: Date ,day: Int) -> Bool {
        
        var filterDate = date.startOfWeek!
        filterDate = Calendar.current.date(byAdding: .day, value: day, to: filterDate)!
        
        var startDate = dateStart.startOfWeek!
        startDate = Calendar.current.date(byAdding: .day, value: day, to: startDate)!
        
        //var endDate = dateEnd.startOfWeek!
        //endDate = Calendar.current.date(byAdding: .day, value: day, to: endDate)!

        if filterDate.compare(dateStart) == .orderedAscending {
            return false
        }
        
        //if dateEnd.compare(endDate) == .orderedAscending {
            //return false
        //}
        
        return true
    }
    
}
