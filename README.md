# BSTU Scheule
![alt text](./Screens/icons.png "Logo")

![Platform](https://img.shields.io/cocoapods/p/JTCalendar.svg)
![Language](https://img.shields.io/badge/russian-100%25-brightgreen.svg)
![Style](https://img.shields.io/badge/style-flat-green.svg?style=flat)
[![Bitbucket open pull requests](https://img.shields.io/bitbucket/pr/osrf/gazebo.svg)]()

## Screenshots

![Example](./Screens/gif1.gif "Example View")
![Example](./Screens/gif11.gif "Example View")
![Example](./Screens/gif111.gif "Example View")

## Overview

This application is for the University of BSTU. Bryansk State University. The application allows you to view the schedule for students and teachers, as well as work to create, edit and delete events. There is support for notifications.

### Plugins

| ------ |
| ------ |
| [RealmSwift](https://github.com/realm/realm-cocoaa) |
| [Alamofire](https://github.com/Alamofire/Alamofire) |
| [JTAppleCalendar](https://github.com/patchthecode/JTAppleCalendar) |

## Requirements

- iOS 10 or higher

## Author

| BarredEwe, barredewe@gmail.com 
| [Instagram](https://www.instagram.com/barredewe/)
| [VK](https://vk.com/barredewe)

## License
----

MIT
