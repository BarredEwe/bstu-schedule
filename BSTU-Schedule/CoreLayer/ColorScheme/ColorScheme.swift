import UIKit

class ColorScheme {
    
    class func mainBackgroundColor() -> UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
    }
    
    class func lightackgroundColor() -> UIColor {
        return UIColor.white
    }
    
    class func darkBackgroundColor() -> UIColor {
        return UIColor.white
    }
    
    class func itemBackgroundColor() -> UIColor {
        return UIColor.white
    }
    
    class func greenItemColor() -> UIColor {
        return UIColor(red: 93/255, green: 206/255, blue: 176/255, alpha: 1.0)
    }
    
    class func orangeItemColor() -> UIColor {
        return UIColor(red: 232/255, green: 161/255, blue: 12/255, alpha: 1.0)
    }
    
    class func darkItemColor() -> UIColor {
        return UIColor(red: 27/255, green: 59/255, blue: 71/255, alpha: 1.0)
    }
    
}
