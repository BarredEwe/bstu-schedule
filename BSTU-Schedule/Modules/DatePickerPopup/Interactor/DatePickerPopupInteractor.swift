import Foundation

class DatePickerPopupInteractor: DatePickerPopupInteractorInput {
    weak var output: DatePickerPopupInteractorOutput!
    
    var objects = [ClassTime]()
    var delegate: SearchResultDelegate?
    
    func loadAllClassTime() {
        APIClassTime.getAllClassTime(success: { objects in
            self.objects = objects!
            self.objects = (objects?.sorted( by: {$0.start! < $1.start!} ))!
            self.output.reloadData()
        }) { error in
            print(error)
        }
    }
    
    func setDelegate(delegate: SearchResultDelegate) {
        self.delegate = delegate
    }
    
    func getAllClassTime() -> [ClassTime]  {
        return objects
    }
    
    func sendResultWith(index: Int) {
        delegate?.didFinisedSearchWith(result: objects[index])
    }

}
