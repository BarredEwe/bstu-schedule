import UIKit

class ScheduleContainerAssembly {
    
    class func assemblyWith(filter: Filter) -> UIPageViewController {
        let storyBoard = UIStoryboard(name: "ScheduleContainerView", bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! ScheduleContainerViewController
        
        let router = ScheduleContainerRouter()
        router.view = vc
        
        let presenter = ScheduleContainerPresenter()
        presenter.registerNotification()
        presenter.view = vc
        presenter.router = router
        
        let interactor = ScheduleContainerInteractor()
        interactor.setFilter(filter: filter)
        interactor.output = presenter
        
        presenter.interactor = interactor
        vc.output = presenter
        return vc
    }
    
}
