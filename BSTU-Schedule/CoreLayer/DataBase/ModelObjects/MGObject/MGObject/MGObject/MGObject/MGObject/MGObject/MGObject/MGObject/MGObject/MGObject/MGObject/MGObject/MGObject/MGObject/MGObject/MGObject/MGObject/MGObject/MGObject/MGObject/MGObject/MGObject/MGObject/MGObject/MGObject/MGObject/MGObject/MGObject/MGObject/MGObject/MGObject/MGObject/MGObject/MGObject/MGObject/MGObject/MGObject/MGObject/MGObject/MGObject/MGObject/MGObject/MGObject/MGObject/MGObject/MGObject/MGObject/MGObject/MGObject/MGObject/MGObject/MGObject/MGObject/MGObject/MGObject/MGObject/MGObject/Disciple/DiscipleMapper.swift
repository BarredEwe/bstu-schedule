import UIKit

protocol DiscipleMapperProtocol {
    static func objectFrom(object: MGObject) -> Disciple
}

class DiscipleMapper: MGObjectMapper, DiscipleMapperProtocol {
    
    class func objectFrom(object: MGObject) -> Disciple {
        let disciple = Disciple()
        disciple.name = object.name
        disciple.id = object.id
        return disciple
    }
    
    class func objectsFrom(objects: [MGObject]) -> [Disciple] {
        var disciples = [Disciple]()
        for object in objects {
            let disciple = Disciple()
            disciple.name = object.name
            disciple.id = object.id
            disciples.append(disciple)
        }
        
        return disciples
    }
    
}
