import JTAppleCalendar

class SectionHeaderView: JTAppleCollectionReusableView {
    
    @IBOutlet var title: UILabel!
    
    func configureWith(range: (start: Date, end: Date)) {
        
        let month = Calendar.current.dateComponents([.month], from: range.start).month!
        
        let monthName = DateFormatter().standaloneMonthSymbols[(month - 1) % 12]
        let year = Calendar.current.component(.year, from: range.start)
        
        title.text = "\(monthName) \(year)"
    }
}
