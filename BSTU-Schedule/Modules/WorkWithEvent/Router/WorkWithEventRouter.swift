import UIKit

class WorkWithEventRouter: WorkWithEventRouterInput {
    
    weak var view: UIViewController?
    
    func searchWith(type: TypeSearch, delegate: SearchResultDelegate) {
        let searchView = SearchAssembly.assemblyWith(type: type, delegate: delegate)
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionFade
        
        self.view?.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.view?.navigationController?.pushViewController(searchView, animated: false)
    }
    
    func showSucсefullMessage(actionOk: @escaping () -> ()) {
        let alertView = UIAlertController(title: "Успех",
                                          message: "Вы успешно создали событие" as String, preferredStyle:.alert)
        
        let noAction = UIAlertAction(title: "Ок", style: .cancel, handler: { action in
            actionOk()
            //self.navigationController?.popViewController(animated: true)
        })
        
        alertView.addAction(noAction)
        view?.present(alertView, animated: true, completion: nil)
    }
    
    func navigationBack() {
        view?.navigationController?.popViewController(animated: true)
    }

    
}
