import Foundation

protocol ScheduleContainerRouterInput {
    func navigateToCalendarWith(filter: Filter, delegate: SearchResultDelegate)
}
