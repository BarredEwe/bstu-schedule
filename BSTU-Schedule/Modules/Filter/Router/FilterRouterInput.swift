import Foundation

protocol FilterRouterInput {
    
    func navigateToCalendarWith(filter: Filter, delegate: SearchResultDelegate)
    func searchWith(type: TypeSearch, delegate: SearchResultDelegate)
    func navigateToShowDayScheduleWith(filter: Filter)

}
