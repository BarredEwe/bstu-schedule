import UIKit

class SearchPresenter: SearchModuleInput, SearchViewOutput, SearchInteractorOutput {

    weak var view: SearchViewInput!
    var interactor: SearchInteractorInput!
    var router: SearchRouterInput!

    func viewIsReady() {
        
    }
    
    func getTitile() -> String {
        return self.interactor.getTitle()
    }
    
    func finisSearch(info: String) {
        self.interactor.sendResult(info: info)
        self.router.navigateBack()
    }
    
    func searchWith(info: String) {
        self.interactor.searchWith(info: info)
    }
    
    func showResult() {
        self.view.updateInfo()
    }
    
    func getObjects() -> [MGObject] {
        return self.interactor.getObjects()
    }
    
    func navigateBack() {
        self.router.navigateBack()
    }
}
