import UIKit

/// Allows you to work with events.
class APIEvent: APIClient {
    /**
     Type of event.
     
     - Regular: For regular events that can be reused within a specific time frame.
     - Single: For single events, which are used only once.
     */
    enum TypeEvent {
        case Regular
        case Single
    }
    
    /**
     Creates an event on the server.
     
     - Parameter event: The event you want to create.
     - Parameter success: Successful event creation.
     - Parameter failure: A failed event creation.
     */
    class func createEvent(event: EventForRespond, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        
        /// The style of the bicycle.
        let URL = "event"
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm"
        let start = formatter.string(from: event.classTime.start!)
        let end = formatter.string(from: event.classTime.end!)
        
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: event.event.startDate!)
        
        //USER
        let userName = UserDB().getUser()!.name
        let user = DiscipleDB().getDesciples(filter: userName)?.first!
        //GROUPS
        let groups = event.event.groups
        var newGroups = [[String: String]]()
        for gr in groups {
            let ggBD = GroupDB().getGroups(filter: gr.name)!.first
            newGroups.append(["id": (ggBD?.id)!, "name": (ggBD?.name)!])
        }
        //TYPE
        if let typeID = LessonTypeDB().getLessonType(filter: (event.event.type?.name)!)?.first?.id {
            event.event.type?.id = typeID
        }
        //SUBJECT
        if let lessonID = LessonDB().getLessons(filter: (event.event.lesson?.name)!)?.first?.id {
            event.event.lesson?.id = lessonID
        }
        //LectureHall
        if let audienceID = RoomDB().getRooms(filter: (event.event.audience?.name)!)?.first?.id {
            event.event.audience?.id = audienceID
        }
        
        let parametres = [
            "subject": ["id" : (event.event.lesson?.id)!,
                        "name": (event.event.lesson?.name)!],
            "lecturer": [["id" : (user?.id)!,
                          "name": (user?.name)!]],
            "lectureHall": ["id" : (event.event.audience?.id)!,
                            "name": (event.event.audience?.name)!],
            "groups": newGroups,
            "eventType": ["id" : (event.event.type?.id)!,
                          "name": (event.event.type?.name)!],
            "classTime": ["id": event.classTime.id,
                          "name": event.classTime.name,
                          "time": ["start": start,
                                   "end": end]],
            "date": ["date": date]
            ] as [String : Any]
        
        let header = ["X-Auth-Token": UserDB().getUser()?.token] as!  Dictionary<String, String>
        
        self.requset(methodType: .post, URL: URL, headers: header, parametres: parametres, success: { responceObject in
            success(responceObject)
        }) { error in
            failure(error)
        }
        
    }
    /**
     Remove an event on the server.
     
     - Parameter eventID: Event ID to be deleted from the server.
     - Parameter success: Successful event deleted.
     - Parameter failure: A failed event deleted.
     */
    class func removeEvent(eventID: String, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        let URL = "event/\(eventID)"
        let header = ["X-Auth-Token": UserDB().getUser()?.token] as!  Dictionary<String, String>
        
        self.requset(methodType: .delete, URL: URL, headers: header, parametres: nil, success: { responceObject in
            success(responceObject)
        }) { error in
            failure(error)
        }
    }
    /**
     Canceling an event on the server.
     
     - Parameter eventID: Event ID to be canceled from the server.
     - Parameter date: Date the event was canceled.
     - Parameter success: Successful event canceled.
     - Parameter failure: A failed event canceled.
     */
    class func cancelEvent(eventID: String, date: Date, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        let URL = "event/cancelLesson"
        let header = ["X-Auth-Token": UserDB().getUser()?.token] as!  Dictionary<String, String>
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let strDate = formatter.string(from: date)
        
        let parametres = ["id": eventID, "date": strDate]
        
        self.requset(methodType: .post, URL: URL, headers: header, parametres: parametres, success: { responceObject in
            success(responceObject)
        }) { error in
            failure(error)
        }
    }
    
    /**
     Cancel the cancellation of the event on the server.
     
     - Parameter changeEventID: Event ID that is canceled.
     - Parameter date: Date the event was canceled.
     - Parameter success: Successful event canceled.
     - Parameter failure: A failed event canceled.
     */
    class func cancelOfCancelEvent(changeEventID: String, date: Date, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        let URL = "event/removeCancelLesson/"
        let header = ["X-Auth-Token": UserDB().getUser()?.token] as!  Dictionary<String, String>
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let strDate = formatter.string(from: date)
        
        let parametres = ["id": changeEventID, "date": strDate]
        
        self.requset(methodType: .post, URL: URL, headers: header, parametres: parametres, success: { responceObject in
            success(responceObject)
        }) { error in
            failure(error)
        }
    }
    /**
     Event transfer on the server.
     
     - Parameter eventID: Event ID to be rescheduled.
     - Parameter date: Date the event was tranfer.
     - Parameter success: Successful event tranfer.
     - Parameter failure: A failed event tranfer.
     */
    class func transferEvent(eventID: String, date: Date, newDate: Date, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        let URL = "event/eventTransfer/"
        
        let header = ["X-Auth-Token": UserDB().getUser()?.token] as!  Dictionary<String, String>
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let strDate = formatter.string(from: date)
        let strNewDate = formatter.string(from: newDate)
        
        let parametres = ["id": eventID, "date": strDate, "newDate": strNewDate]
        
        self.requset(methodType: .post, URL: URL, headers: header, parametres: parametres, success: { responceObject in
            success(responceObject)
        }) { error in
            failure(error)
        }
    }
    /**
     Undo an event transfer.
     
     - Parameter changeEventID: Event ID to be rescheduled.
     - Parameter success: Successful event untranfer.
     - Parameter failure: A failed event untranfer.
     */
    class func transferCancelEvent(changeEventID: String, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        let URL = "event/removeEventTransfer"
        
        let header = ["X-Auth-Token": UserDB().getUser()?.token] as!  Dictionary<String, String>
        
        let parametres = ["id": changeEventID]
        
        self.requset(methodType: .post, URL: URL, headers: header, parametres: parametres, success: { responceObject in
            success(responceObject)
        }) { error in
            failure(error)
        }
    }
    
}
