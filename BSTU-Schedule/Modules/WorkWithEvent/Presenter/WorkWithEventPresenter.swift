import UIKit

class WorkWithEventPresenter: WorkWithEventModuleInput, WorkWithEventViewOutput, WorkWithEventInteractorOutput {

    weak var view: WorkWithEventViewInput!
    var interactor: WorkWithEventInteractorInput!
    var router: WorkWithEventRouterInput!

    func viewIsReady() {

    }
    
    func searchWith(type: TypeSearch) {
        //self.router.searchWith(type: type, delegate: self)
    }
    
    func didFinisedSearchWith(date: Date) {
        
    }
    
    func didFinisedSearchWith(result: String) {
        
    }
    
    func showSucсefullMessage() {
        self.router.showSucсefullMessage { [unowned self] in
            self.router.navigationBack()
        }
    }
}
