import RealmSwift

class LessonDB: MGObjectDB {
    
    func fetchLessons<T: MGObject>(lessons: [T]) {
        let newLessons = LessonMapper.objectsFrom(objects: lessons)
        super.fetchObjects(groups: newLessons)
    }
    
    func getLessons(filter: String) -> [Lesson]? {
        return super.getObjects(filter: filter, type: Lesson())
    }
    
}
