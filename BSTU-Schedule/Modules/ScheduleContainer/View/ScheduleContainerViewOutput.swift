import UIKit

protocol ScheduleContainerViewOutput {
    
    func viewIsReady()
    func showCalendar()
    func createPages()
    func getPageBefore(viewController: UIViewController) -> UIViewController?
    func getPageAfter(viewController: UIViewController) -> UIViewController?
    func getPages() -> [UIViewController]
}
