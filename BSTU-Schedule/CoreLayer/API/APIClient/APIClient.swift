import Foundation
import Alamofire

private var isNowRequested: Bool?
var token: String?

let mBaseURL = "http://95.213.251.173:3000/"

/// The main class for sending requests to the server
class APIClient: APIClientProtocol {
    
    class func requset(methodType: HTTPMethod, URL: String, baseURL: String = mBaseURL, headers: Dictionary <String, String>?, parametres: Dictionary <String, Any>?, success: @escaping (AnyObject) -> Void, failure: @escaping (NSError) -> Void) {
        
        let URLSend = baseURL + URL
        let allowedCharacterSet = NSCharacterSet.urlQueryAllowed
        let send = URLSend.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)
        
        print(URLSend)
        if parametres != nil {
            print(parametres!)
        }
        
        Alamofire.request(send!, method: methodType, parameters: parametres, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                switch response.result {
                case .success(let JSON):
                    success(JSON as AnyObject)
                case .failure(let error):
                    failure(error as NSError)
            }
        }
    }

}

