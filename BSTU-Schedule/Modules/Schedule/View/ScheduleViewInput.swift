import UIKit

protocol ScheduleViewInput: class {

    func setupInitialState()
    func updateSchedule()

}
