import XCTest
@testable import BSTU_Schedule

let baseURL = "https://jsonplaceholder.typicode.com/"

class APIClientTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGET() {
        let expect = expectation(description: "Timeout wating")
        let kGETEndPoint = "posts/1"
        
        APIClient.requset(methodType: .get, URL: kGETEndPoint, baseURL: baseURL, headers: nil, parametres: nil, success: { responseObject in
            let dictionaryObject = responseObject as! NSDictionary
            XCTAssertNotNil(dictionaryObject["userId"], "Key id not contained")
            expect.fulfill()
        }, failure: { error in
            XCTAssertNil(error, "Error")
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssertNil(error, "Error")
        }
    }
    
    func testPOST() {
        let expect = expectation(description: "Timeout wating")
        let kGETEndPoint = "posts"
        
        APIClient.requset(methodType: .post, URL: kGETEndPoint, baseURL: baseURL, headers: nil, parametres: nil, success: { responseObject in
            let dictionaryObject = responseObject as! NSDictionary
            XCTAssertNotNil(dictionaryObject["id"], "Key id not contained")
            expect.fulfill()
        }, failure: { error in
            XCTAssertNil(error, "Error")
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssertNil(error, "Error")
        }
    }
    
    func testDELETE() {
        let expect = expectation(description: "Timeout wating")
        let kGETEndPoint = "posts/1"
        
        APIClient.requset(methodType: .delete, URL: kGETEndPoint, baseURL: baseURL, headers: nil, parametres: nil, success: { responseObject in
            expect.fulfill()
        }, failure: { error in
            XCTAssertNil(error, "Error")
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssertNil(error, "Error")
        }
    }
    
    func testPUT() {
        let expect = expectation(description: "Timeout wating")
        let kGETEndPoint = "posts/1"
        
        APIClient.requset(methodType: .put, URL: kGETEndPoint, baseURL: baseURL, headers: nil, parametres: nil, success: { responseObject in
            expect.fulfill()
        }, failure: { error in
            XCTAssertNil(error, "Error")
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssertNil(error, "Error")
        }
    }
    
    func testPATCH() {
        let expect = expectation(description: "Timeout wating")
        let kGETEndPoint = "posts/1"
        
        APIClient.requset(methodType: .patch, URL: kGETEndPoint, baseURL: baseURL, headers: nil, parametres: nil, success: { responseObject in
            expect.fulfill()
        }, failure: { error in
            XCTAssertNil(error, "Error")
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssertNil(error, "Error")
        }
    }
    
}
