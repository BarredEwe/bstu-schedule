import Foundation

protocol PersonalAreaInteractorInput {
    
    func logoutUser()
    func clearChache()
    
}
