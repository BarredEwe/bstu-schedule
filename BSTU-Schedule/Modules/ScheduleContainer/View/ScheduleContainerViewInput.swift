//
//  ScheduleContainerScheduleContainerViewInput.swift
//  BSTU-Schedule
//
//  Created by Maxim on 10/04/2017.
//  Copyright © 2017 Maksim Grishutin. All rights reserved.
//

protocol ScheduleContainerViewInput: class {
    func setupInitialState()
    func refreshPageViewController()
}
