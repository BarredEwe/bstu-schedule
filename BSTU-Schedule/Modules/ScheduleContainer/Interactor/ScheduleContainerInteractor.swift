import  UIKit

class ScheduleContainerInteractor: ScheduleContainerInteractorInput {

    weak var output: ScheduleContainerInteractorOutput!
    var pages = [UIViewController?]()
    var filter: Filter?
    var filters = [Filter]()
    
    let numberOfDays = 4
    let daysInWeek = 7
    
    func setFilter(filter: Filter) {
        self.filter = filter
    }
    
    func setFilter(day: Date) {
        self.filter?.day = day
    }
    
    func getFilter() -> Filter {
        return filter!
    }
    
    func getPages() -> [UIViewController] {
        return pages as! [UIViewController]
    }
    
    func deinitPreviousControllers(index: Int) {
        if index > 4 {
            for id in 0...index - 3 {
                pages[id] = nil
            }
        }
        
        if index + 4 < pages.count {
            for id in index + 3...pages.count - 1 {
                pages[id] = nil
            }
        }
        print(index," ", pages.count)
    }
    
    func getIndexOf(viewController: UIViewController) -> Int {
        var index = 0
        
        for vc in pages {
            if vc == viewController {
                return index
            }
            index += 1
        }
        return 0
    }
    
    //Следующая
    func getPageAfter(viewController: UIViewController) -> UIViewController? {
        let index = getIndexOf(viewController: viewController)
        deinitPreviousControllers(index: index)
        return getPage(index: index + 1)
    }
    
    //Предыдущая
    func getPageBefore(viewController: UIViewController) -> UIViewController? {
        let index = getIndexOf(viewController: viewController)
        deinitPreviousControllers(index: index)
        return index == 0 ? nil : getPage(index: index - 1)
    }
    
    func getPage(index: Int) -> UIViewController {
        if pages.count > index {
            if pages[index] != nil {
                return pages[index]!
            }
        } else {
            self.pages.append(createNewPageWith(index: index))
            return pages[index]!
        }
        self.pages[index] = createNewPageWith(index: index)
        return pages[index]!
    }
    
    func createNewPageWith(index: Int) -> UIViewController {
        var filter = Filter()
        if filters.count < index {
            filter = filters[index]
        } else {
            filter.data = (self.filter?.data)!
            filter.type = (self.filter?.type)!
            filter.day = Calendar.current.date(byAdding: .day, value: (index) * daysInWeek, to: (self.filter?.day)!)!
            filters.append(filter)
        }
        
        return ScheduleAssembly.assemblyWith(filter: filters[index])
    }
    
    func createPages() {
        pages.removeAll()
        filters.removeAll()
        
        for i in 0...numberOfDays {
            let filter = Filter()
            filter.data = (self.filter?.data)!
            filter.type = (self.filter?.type)!
            filter.day = Calendar.current.date(byAdding: .day, value: i * daysInWeek, to: (self.filter?.day)!)!
            filters.append(filter)
            
            let page = ScheduleAssembly.assemblyWith(filter: filters[i])
            self.pages.append(page)
        }
    }
    
}
