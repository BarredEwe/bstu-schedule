import UIKit
import RealmSwift

class Day: Object {
    
    @objc dynamic var date: Date?
    var events = List<Event>()
    var eventsOther = List<Event>()
    
}
