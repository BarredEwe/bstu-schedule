import UIKit

class DataCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var teacher: UILabel!
    @IBOutlet weak var group: UILabel!
    @IBOutlet weak var audience: UILabel!
    @IBOutlet weak var isNow: UILabel!
    
    private var event = Event()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(event: Event) {
        self.event = event
        let dF = DateFormatter()
        dF.dateFormat = "HH:mm"
        dF.string(from: Date())
        
        self.startTime.text = dF.string(from: event.startTime!)
        self.endTime.text = dF.string(from: event.endTime!)
        self.type.text = event.type?.name
        self.type.layer.cornerRadius = 2
        self.name.text = event.lesson?.name
        self.teacher.text = event.teachers.first?.name
        self.group.text = event.groups.first?.name
        self.audience.text = event.audience?.name
        
        switch (event.type?.name)! {
        case "Практика":
            self.type.backgroundColor = ColorScheme.darkItemColor()
            break
        case "Лекция":
            self.type.backgroundColor = ColorScheme.greenItemColor()
            break
        default:
            self.type.backgroundColor = ColorScheme.orangeItemColor()
        }
        
//        Если текущий экземпляр представляет более позднюю дату, чем экземпляр anotherDate, метод вернет значение соответствующие константе NSOrderedDescending
//        Если текущий экземпляр представляет более ранюю дату и время, чем экземпляр anotherDate, метод вернет значение соответствующие константе NSOrderedAscending
        
        if event.startTime?.compare(Date()) == .orderedAscending && Date().compare(event.endTime!) == .orderedAscending {
            isNow.isHidden = false
        }
        
        if event.changedInfo != "" {
            //isNow.isHidden = false
            type.text = event.changedInfo
        }
    }
    
    func getEvent() -> Event {
        return event
    }
    
}
