import UIKit

class ExtendedScheduleViewController: UIViewController, ExtendedScheduleViewInput, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SearchResultDelegate {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var room: UILabel!
    
    var output: ExtendedScheduleViewOutput!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupHeader()
        self.setupNavigationBar()
        output.viewIsReady()
    }
    
    
    // MARK: ExtendedScheduleViewInput
    func setupInitialState() {
        
    }
    
    
    func setupNavigationBar() {
        self.view.backgroundColor = ColorScheme.mainBackgroundColor()
        if APIUser.chechAuthorize() && UserDB().getUser()?.name == self.output.getEvent().teachers.first?.name {
            let rightButton = UIBarButtonItem(image: UIImage(named: "Settings"), style: .plain, target: self, action: #selector(touchSettingBarButton))
            self.navigationItem.rightBarButtonItem = rightButton
        }
    }
    
    func setupHeader() {
        self.name.text = self.output.getEvent().lesson?.name
        self.type.text = self.output.getEvent().type?.name
        self.room.text = self.output.getEvent().audience?.name
        
        let dF = DateFormatter()
        dF.dateFormat = "HH:mm"
        
        let start = dF.string(from: self.output.getEvent().startTime!)
        let end = dF.string(from: self.output.getEvent().endTime!)
        
        self.time.text = "\(start) - \(end)"
    }
    
    @objc func touchSettingBarButton() {
        let alertView = UIAlertController(title: "Настройка события",
                                          message: "Выберите, что нужно сделать", preferredStyle:.alert)
        
        let transferAction = UIAlertAction(title: "Перенести событие", style: .default, handler: { action in
            self.transferEvent()
        })
        alertView.addAction(transferAction)
        
        if !self.output.getEvent().single {
            let cancelAction = UIAlertAction(title: "Отменить событие", style: .default, handler: { action in
                self.cancelEvent()
            })
            alertView.addAction(cancelAction)
        }
        
        let deleteAction = UIAlertAction(title: "Удалить событие", style: .default, handler: { actin in
            self.removeEvent()
        })
        alertView.addAction(deleteAction)
        
        let noAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alertView.addAction(noAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    func showAtertMessage(title: String, message: String, completion: ((UIAlertAction) -> ())? = nil) {
        let alertView = UIAlertController(title: title,
                                          message: message, preferredStyle:.alert)
        
        let noAction = UIAlertAction(title: "Ок", style: .cancel, handler: completion)
        alertView.addAction(noAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    func transferEvent() {
        let event = self.output.getEvent()
        if event.idEvent == "" {
            let vc = CalendarAssembly.assemblyWith(filter: self.output.getFilter(), delegate: self)
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(vc, animated: true, completion: nil)
        } else {
            APIEvent.transferCancelEvent(changeEventID: event.idEvent, success: { object in
                print(object)
                self.showAtertMessage(title: "Успех", message: "Перенос события успешно отменен!", completion: { action in
                    self.output.navigationBack()
                })
                
            }, failure: { erro in
                self.showAtertMessage(title: "Ошибка", message: "Ошибка соединения")
            })
        }
    }
    
    func didFinisedSearchWith(result: Any) {
        
        let newDate = result as! Date
        let event = self.output.getEvent()
        let filter = self.output.getFilter()
        
        var date = filter.day!
        date = Calendar.current.date(byAdding: .day, value: event.day, to: date)!
        
        if event.single == true {
            date = (event.startDate?.startOfWeek!)!
            date = Calendar.current.date(byAdding: .day, value: event.day, to: date)!
        }
        
            APIEvent.transferEvent(eventID: self.output.getEvent().id, date: date, newDate: newDate, success: { object in
                print(object)
                self.showAtertMessage(title: "Успех", message: "Событие успешно перенесено!", completion: { action in
                    self.output.navigationBack()
                })
            }, failure: { error in
                self.showAtertMessage(title: "Ошибка", message: "Ошибка соединения")
            })
        
    }
    
    func removeEvent() {
        APIEvent.removeEvent(eventID: self.output.getEvent().id, success: { object in
            print(object)
            self.showAtertMessage(title: "Успех", message: "Событие успешно удалено!", completion: { action in
                self.output.navigationBack()
            })
        }, failure: { error in
            self.showAtertMessage(title: "Ошибка", message: "Ошибка соединения")
        })
    }
    
    func cancelEvent() {
        let filter = self.output.getFilter()
        let event = self.output.getEvent()
        
        var date = filter.day?.startOfWeek!
        date = Calendar.current.date(byAdding: .day, value: event.day, to: date!)!
        
        if event.idEvent == "" {
            APIEvent.cancelEvent(eventID: event.id, date: date!, success: { object in
                print(object)
                self.showAtertMessage(title: "Успех", message: "Событие успешно отменено!", completion: { action in
                    self.output.navigationBack()
                })
            }, failure: { error in
                self.showAtertMessage(title: "Ошибка", message: "Ошибка соединения")
            })
        } else {
            if event.changedInfo == "Событие перенесено!"{
                self.transferEvent()
                return
            }
            
            APIEvent.cancelOfCancelEvent(changeEventID: event.id, date: date!, success: { object in
                print(object)
                self.showAtertMessage(title: "Успех", message: "Отмена события отменена!", completion: { action in
                    self.output.navigationBack()
                })
            }, failure: { error in
                self.showAtertMessage(title: "Ошибка", message: "Ошибка соединения")
            })
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.output.getEvent().teachers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeacherCell", for: indexPath) as! TeacherCell
        cell.teacherLabel.text = self.output.getEvent().teachers[indexPath.item].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var cell: UICollectionReusableView?
        if kind == UICollectionElementKindSectionHeader {
            cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ExtendedHeader", for: indexPath)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 35)
    }
    
    @IBAction func notificationButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}
