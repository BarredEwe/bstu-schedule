import UIKit

protocol SendSearchDelegate {
    func startSearchWith(data: String, type: TypeSearch)
}

struct EventForRespond {
    let event = Event()
    var classTime = ClassTime()
}

class WorkWithEventViewController: UITableViewController, WorkWithEventViewInput, UITextFieldDelegate, SearchResultDelegate {

    var output: WorkWithEventViewOutput!
    var isDate = true
    @IBOutlet var weeks: [UIButton]!
    var searchResultsView: SearchResultPopUpView!
    var delegateSearch: SendSearchDelegate?
    var type: TypeSearch = .disciple
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var buttons: [UIButton]!
    var lastSearchIndex = 0
    
    var event = EventForRespond()
        
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        searchResultsView = SearchResultPopUpViewAssembly.assemblyWith(searchDelegate: self)
        delegateSearch = searchResultsView
        searchResultsView.isHidden = true
        self.view.addSubview(searchResultsView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    // MARK: WorkWithEventViewInput
    func setupInitialState() {
        
    }
    
    // MARK: Keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.tableView.bounds.origin.x == 0 {
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.tableView.frame.origin.y -= keyboardSize.height / 3
//                })
//            }
//        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.tableView.frame.origin.y != 0 {
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.tableView.frame.origin.y += keyboardSize.height / 3
//                })
//            }
//        }
    }

    // MARK: TableView
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && !isDate {
            return 0
        }
        if indexPath.section == 2 && isDate{
            return 0
        }
        return 60
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        showSearchResult(sender: sender)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchResultsView.isHidden = true
    }
  
    @IBAction func segmentedControllChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            isDate = true
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        } else {
            isDate = false
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
    
    func showSearchResult(sender: UITextField) {
        let cellFrame = (sender.superview?.superview?.frame)!
        let frame = CGRect(x: cellFrame.origin.x, y: cellFrame.origin.y + cellFrame.height, width: cellFrame.width, height: cellFrame.height * 2)
        searchResultsView.frame = frame
        
        didStartSearch(sender: sender)
        
        if sender.text != "" {
            searchResultsView.isHidden = false
        } else {
            searchResultsView.isHidden = true
        }
    }
    
    func didStartSearch(sender: UITextField) {
        switch sender.tag {
        case 0:
            type = .lesson
            break
        case 1:
            type = .group
            break
        case 2:
            type = .room
            break
        default:
            type = .typeLesson
        }
        
        delegateSearch?.startSearchWith(data: sender.text!, type: type)
    }
    
    func didFinisedSearchWith(result: Any) {
        
        let formatter = DateFormatter()
        
        if result is ClassTime {
            event.classTime = result as! ClassTime
            
            formatter.dateFormat = "HH:mm"
            let dateStr = formatter.string(from: event.classTime.start!)
            
            buttons[3].setTitle(dateStr, for: .normal)
            buttons[3].isSelected = true
            return
        }
        
        if result is Date {
            switch lastSearchIndex {
            case 0:
                formatter.dateFormat = "EEE d MMM YYYY"
                let dateStr = formatter.string(from: result as! Date)
                
                buttons[0].setTitle(dateStr, for: .normal)
                buttons[0].isSelected = true
                
                event.event.startDate = result as? Date
                
                break
            case 1:
                formatter.dateFormat = "HH:mm"
                let dateStr = formatter.string(from: result as! Date)
                
                buttons[3].setTitle(dateStr, for: .normal)
                buttons[3].isSelected = true
                
                event.classTime.start = result as? Date
                let newdate = Calendar.current.date(byAdding: .minute, value: 135, to: event.classTime.start!)
                event.classTime.end = newdate
                
                break
            default:
                formatter.dateFormat = "EEE d MMM YYYY"
                let dateStr = formatter.string(from: result as! Date)
                
                buttons[1].setTitle(dateStr, for: .normal)
                buttons[1].isSelected = true
                break
            }
            return
        }
        
        var row = 0
        switch type {
        case .lesson:
            let lesson = Lesson()
            lesson.name = result as! String
            event.event.lesson = lesson
            row = 0
            break
        case .group:
            let group = Group()
            group.name = result as! String
            event.event.groups.removeAll()
            event.event.groups.append(group)
            row = 1
            break
        case .room:
            let room = Room()
            room.name = result as! String
            event.event.audience = room
            row = 2
            break
        case .typeLesson:
            let typeLesson = LessonType()
            typeLesson.name = result as! String
            event.event.type = typeLesson
            row = 3
            break
        default:
            break
        }
        textFields[row].text = result as? String
        textFields[row].resignFirstResponder()
        searchResultsView.isHidden = true
    }
    
    @IBAction func chooseAnyDate(_ sender: UIButton) {
        lastSearchIndex = 0
        
        let vc = CalendarAssembly.assemblyWith(filter: Filter(), delegate: self)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func chooseDate(_ sender: UIButton) {
        
    }
    
    @IBAction func chooseAnyTime(_ sender: Any) {
        lastSearchIndex = 1
        
        let vc = DatePickerPopupAssembly.assemblyWith(searchDelegate: self)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func createEvent(_ sender: UIButton) {
        if !checkIsEmptyFields() {
            showErrorMessage(error: "Не все поля заполнены")
            return
        }
        
        APIEvent.createEvent(event: event, success: { object in
            let str = object as! [String: Any]
            if str["errors"] == nil {
                self.output.showSucсefullMessage()
                return
            } else {
                var error = ""
                for errors in str["errors"] as! [String: Any]{
                    let value = errors.value as! [String: String]
                    error = error + value.values.first!
                }
                self.showErrorMessage(error: error)
            }
        }, failure: { error in
            
        })
    }
    
    func checkIsEmptyFields() -> Bool {
        if event.event.audience == nil {
            return false
        }
        
        if event.event.groups.count == 0 {
            return false
        }
        
        if event.event.lesson == nil {
            return false
        }
        
        if event.event.startDate == nil {
            return false
        }
        
        if event.event.type == nil {
            return false
        }
        
        if event.classTime.start == nil {
            return false
        }
        return true
    }
    
    func showErrorMessage(error: String) {
        let alertView = UIAlertController(title: "Ошибка",
                                          message: error, preferredStyle:.alert)
        
        let noAction = UIAlertAction(title: "Ок", style: .cancel, handler: nil)
        alertView.addAction(noAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
}
