import Foundation

protocol SignInInteractorInput {

    func userAuthorization(userName: String, password: String)
    
}
