import UIKit

let CONTENT_HEIGHT = UIScreen.main.bounds.height
let CONTENT_WIDTH = UIScreen.main.bounds.width
let INVISIBLE_CENTER = CGPoint(x: (UIScreen.main.bounds.width / 2) / 2, y: (UIScreen.main.bounds.height / 2) / 2)
let VISIBLE_CENTER = CGPoint(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2)
let ANIMATION_DURATION = 0.3

protocol PopUpViewDataSource {
    func viewController(forPresentContent view: PopUpView) -> UIViewController
}

class PopUpView {
    
    var dataSource: PopUpViewDataSource?
    
    var window: UIWindow?
    
    init(size: CGSize) {
        self.window = UIWindow(frame: CGRect(origin: CGPoint.zero, size: size))
        self.window?.isHidden = true
        self.window?.backgroundColor = UIColor.clear
        self.window?.center = INVISIBLE_CENTER
    }
    
    func present() {
        let vc = self.dataSource?.viewController(forPresentContent: self)
        self.window?.rootViewController = vc
        self.window?.isHidden = false
        self.window?.makeKeyAndVisible()
        
        UIView.animate(withDuration: ANIMATION_DURATION) { 
            self.window?.center = VISIBLE_CENTER
        }
    }
    
    func dismiss() {
        self.window?.isHidden = false
        self.window?.makeKeyAndVisible()
        UIView.animate(withDuration: ANIMATION_DURATION, animations: {
            self.window?.center = INVISIBLE_CENTER
        }) { (finished) in
            self.window?.isHidden = true
        }
    }
    
}
