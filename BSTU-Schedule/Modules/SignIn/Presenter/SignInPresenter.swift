import UIKit

class SignInPresenter: SignInModuleInput, SignInViewOutput, SignInInteractorOutput {

    weak var view: SignInViewInput!
    var interactor: SignInInteractorInput!
    var router: SignInRouterInput!

    func viewIsReady() {

    }
    
    func closeView() {
        self.router.closeView()
    }
    
    func showErrorAuthorizationAlert() {
        self.router.showErrorAuthorizationAlert()
    }
    
    func userAuthorization(userName: String, password: String) {
        self.interactor.userAuthorization(userName: userName, password: password)
    }
    
    func changeLoadingState() {
        self.view.changeLoadingState()
    }
    
}
